package com.farmhand.farmjoint.ui.supplement

import android.app.DatePickerDialog
import android.graphics.Color
import android.os.Bundle
import android.text.TextUtils
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.DatePicker
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.lifecycle.observe
import com.farmhand.farmjoint.FarmJoint
import com.farmhand.farmjoint.R
import com.farmhand.farmjoint.data.model.Supplement
import com.farmhand.farmjoint.databinding.FragmentAddSupplementBinding
import com.farmhand.farmjoint.ui.feed.viewModel.FeedViewModel
import com.farmhand.farmjoint.ui.supplement.viewModel.SupplementViewModel
import com.farmhand.farmjoint.utils.Resource
import com.farmhand.farmjoint.utils.autoCleared
import com.github.razir.progressbutton.attachTextChangeAnimator
import com.github.razir.progressbutton.bindProgressButton
import com.github.razir.progressbutton.hideProgress
import com.github.razir.progressbutton.showProgress
import com.google.gson.Gson
import com.google.gson.JsonObject
import dagger.hilt.android.AndroidEntryPoint
import okhttp3.MediaType.Companion.toMediaType
import okhttp3.RequestBody.Companion.toRequestBody
import java.text.SimpleDateFormat
import java.util.*

@AndroidEntryPoint
class AddSupplement : Fragment() {
    private var binding: FragmentAddSupplementBinding by autoCleared()
    private val viewModel: SupplementViewModel by viewModels()


    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentAddSupplementBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val addBtn = binding.addSupplement
        bindProgressButton(addBtn)
        addBtn.attachTextChangeAnimator()
        val myCalendar = Calendar.getInstance()

        val date =
            DatePickerDialog.OnDateSetListener { _: DatePicker?, year: Int, monthOfYear: Int, dayOfMonth: Int ->
                myCalendar.set(Calendar.YEAR, year)
                myCalendar.set(Calendar.MONTH, monthOfYear)
                myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth)
                val myFormat = "yyyy-MM-dd"
                val sdf =
                    SimpleDateFormat(myFormat, Locale.getDefault())
                binding.dateAdded.setText(sdf.format(myCalendar.time))
            }

        binding.dateAdded.setOnClickListener {
            DatePickerDialog(
                requireActivity(), date, myCalendar
                    .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                myCalendar.get(Calendar.DAY_OF_MONTH)
            ).show()
        }

        addBtn.setOnClickListener {
            val dateAdded = binding.dateAdded.text.toString().trim()
            val name = binding.supplementName.text.toString().trim()
            val unit = binding.supplementUnit.text.toString().trim()
            val quantity = binding.supplementQuantity.text.toString().trim()
            val price = binding.supplementPrice.text.toString().trim()

            when {
                TextUtils.isEmpty(dateAdded) -> {
                    binding.dateLayout.error = "Choose Date"
                    binding.dateAdded.requestFocus()
                }
                TextUtils.isEmpty(name) -> {
                    binding.supplementNameLayout.error = "Add Supplement Name"
                    binding.supplementName.requestFocus()
                }
                TextUtils.isEmpty(unit) -> {
                    binding.supplementUnitLayout.error = "Add Supplement Unit"
                    binding.supplementUnit.requestFocus()
                }
                TextUtils.isEmpty(quantity) -> {
                    binding.supplementQuantityLayout.error = "Add Supplement Quantity"
                    binding.supplementQuantity.requestFocus()
                }
                TextUtils.isEmpty(price) -> {
                    binding.supplementPriceLayout.error = "Add Supplement Price"
                    binding.supplementPrice.requestFocus()
                }
                else -> {
                    val supplement = Supplement(
                        price = price,
                        quantity = quantity,
                        units = unit,
                        supplement_name = name,
                        id = "0",
                        date_bought = dateAdded
                    )
                    val json = JsonObject()
                    val jsonElement = Gson().toJsonTree(supplement)
                    json.add("supplement", jsonElement)
                    val mediaType = "application/json; charset=utf-8".toMediaType()
                    val requestBody = json.toString().toRequestBody(mediaType)

                    viewModel.addSupplement(requestBody, "Bearer " + FarmJoint.prefs?.getUserID()!!)
                        .observe(viewLifecycleOwner, Observer {
                            when (it.status) {
                                Resource.Status.SUCCESS -> {
                                    addBtn.hideProgress(R.string.done)
                                    Toast.makeText(
                                        activity,
                                        "feed added successfully",
                                        Toast.LENGTH_LONG
                                    ).show()
                                    binding.dateAdded.setText("")
                                    binding.supplementName.setText("")
                                    binding.supplementUnit.setText("")
                                    binding.supplementQuantity.setText("")
                                    binding.supplementPrice.setText("")
                                }
                                Resource.Status.ERROR -> {
                                    addBtn.hideProgress(R.string.add_produce_button)
                                    Toast.makeText(requireContext(), it.message, Toast.LENGTH_SHORT)
                                        .show()
                                }

                                Resource.Status.LOADING -> {
                                    addBtn.showProgress {
                                        buttonTextRes = R.string.loading
                                        progressColor = Color.WHITE
                                    }
                                }
                            }
                        })
                }
            }
        }
    }

}