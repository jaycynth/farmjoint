package com.farmhand.farmjoint.ui.produce

import android.annotation.SuppressLint
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import com.farmhand.farmjoint.FarmJoint
import com.farmhand.farmjoint.databinding.FragmentProduceDetailsBinding
import com.farmhand.farmjoint.ui.produce.viewModel.ProduceViewModel
import com.farmhand.farmjoint.utils.Resource
import com.farmhand.farmjoint.utils.autoCleared
import dagger.hilt.android.AndroidEntryPoint

@SuppressLint("SetTextI18n")
@AndroidEntryPoint
class ProduceDetails : Fragment() {

    private var binding: FragmentProduceDetailsBinding by autoCleared()
    private val viewModel: ProduceViewModel by viewModels()

    private lateinit var produceId: String
    private lateinit var farmAnimalId: String

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?
    ): View {
        binding = FragmentProduceDetailsBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        produceId = ProduceDetailsArgs.fromBundle(requireArguments()).produceID

        binding.productId.text = "# $produceId"
        setupObservers()
    }

    private fun setupObservers() {

        viewModel.allFarmAnimals("Bearer " + FarmJoint.prefs?.getUserID()!!)
            .observe(viewLifecycleOwner, Observer {
                when (it.status) {
                    Resource.Status.SUCCESS -> {
                        binding.progressBar.visibility = View.GONE

                        for (farmAnimal in it.data!!) {
                            if (farmAnimal.id == farmAnimalId) {
                                binding.farmAnimalTag.text = farmAnimal.tag
                                binding.status.text = farmAnimal.status
                            }
                        }
                    }
                    Resource.Status.ERROR -> {
                        binding.progressBar.visibility = View.GONE

                        Toast.makeText(requireContext(), it.message, Toast.LENGTH_SHORT).show()
                    }

                    Resource.Status.LOADING -> {
                        binding.progressBar.visibility = View.VISIBLE
                    }
                }
            })

        viewModel.allProduce("Bearer " + FarmJoint.prefs?.getUserID()!!)
            .observe(viewLifecycleOwner, Observer {
                when (it.status) {
                    Resource.Status.SUCCESS -> {
                        binding.progressBar.visibility = View.GONE
                        for (produce in it.data!!) {
                            if (produce.id == produceId) {
                                binding.date.text = produce.date
                                binding.productType.text = produce.produce_type
                                binding.quantity.text = produce.quantity.toString()
                                binding.eachPrice.text = "Ksh ${produce.each_price}"
                                binding.totalAmount.text = "Ksh ${produce.total_price}"
                                binding.quantityType.text = produce.quantity_type
                                farmAnimalId = produce.farmanimal_id
                            }
                        }
                    }
                    Resource.Status.ERROR ->
                        Toast.makeText(requireContext(), it.message, Toast.LENGTH_SHORT).show()

                    Resource.Status.LOADING ->
                        binding.progressBar.visibility = View.VISIBLE
                }
            })


    }
}