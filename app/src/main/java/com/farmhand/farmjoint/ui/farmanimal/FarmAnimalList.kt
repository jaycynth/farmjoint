package com.farmhand.farmjoint.ui.farmanimal

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.ItemTouchHelper
import androidx.recyclerview.widget.RecyclerView
import co.mobiwise.materialintro.shape.Focus
import co.mobiwise.materialintro.shape.FocusGravity
import co.mobiwise.materialintro.shape.ShapeType
import co.mobiwise.materialintro.view.MaterialIntroView
import com.farmhand.farmjoint.FarmJoint.Companion.prefs
import com.farmhand.farmjoint.R
import com.farmhand.farmjoint.adapter.FarmAnimalAdapter
import com.farmhand.farmjoint.data.model.FarmAnimal
import com.farmhand.farmjoint.databinding.FragmentFarmAnimalListBinding
import com.farmhand.farmjoint.ui.farmanimal.viewModel.FarmAnimalViewModel
import com.farmhand.farmjoint.utils.Resource
import com.farmhand.farmjoint.utils.autoCleared
import dagger.hilt.android.AndroidEntryPoint


@AndroidEntryPoint
class FarmAnimalList : Fragment(), FarmAnimalAdapter.FarmAnimalItemListener {
    private var binding: FragmentFarmAnimalListBinding by autoCleared()
    private lateinit var adapter: FarmAnimalAdapter
    private val viewModel: FarmAnimalViewModel by viewModels()
    private var farmAnimalList: List<FarmAnimal>? = null

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?
    ): View {
        binding = FragmentFarmAnimalListBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupRecyclerView()
        setupObservers()
        initSwipe()
        binding.addFarmAnimal.setOnClickListener { findNavController().navigate(R.id.action_farmAnimalList_to_addFarmAnimal) }

    }

    private fun initSwipe() {
        val helper = ItemTouchHelper(object : ItemTouchHelper.SimpleCallback(
            0,
            ItemTouchHelper.LEFT or ItemTouchHelper.RIGHT
        ) {
            override fun onMove(
                recyclerView: RecyclerView, viewHolder: RecyclerView.ViewHolder,
                target: RecyclerView.ViewHolder
            ): Boolean {
                return false
            }

            override fun onSwiped(viewHolder: RecyclerView.ViewHolder, direction: Int) {
                val position = viewHolder.bindingAdapterPosition
                val farmAnimal: FarmAnimal = adapter.getCountedItemAtPosition(position)


                // Delete the counted item
                viewModel.deleteFarmAnimal("Bearer " + prefs?.getUserID(), farmAnimal.id).observe(
                    viewLifecycleOwner,
                    Observer {
                        when (it.status) {
                            Resource.Status.SUCCESS -> {
                                Toast.makeText(
                                    requireActivity(),
                                    it.data?.success_message + farmAnimal.tag,
                                    Toast.LENGTH_LONG
                                ).show()
                            }
                            Resource.Status.ERROR ->
                                Toast.makeText(requireContext(), it.message, Toast.LENGTH_SHORT)
                                    .show()

                            Resource.Status.LOADING ->
                                Toast.makeText(
                                    requireContext(),
                                    "Deleting item....",
                                    Toast.LENGTH_SHORT
                                ).show()

                        }
                    })
                farmAnimalList?.toMutableList()?.removeAt(position)
                adapter.notifyItemRemoved(position)

            }
        })

        helper.attachToRecyclerView(binding.farmAnimalRv)
    }

    private fun setupObservers() {
        viewModel.allFarmAnimals("Bearer " + prefs?.getUserID()!!).observe(
            viewLifecycleOwner,
            Observer {
                when (it.status) {
                    Resource.Status.SUCCESS -> {
                        binding.progressBar.visibility = View.GONE
                        if (!it.data.isNullOrEmpty()) {
                            adapter.setItems(ArrayList(it.data))
                            farmAnimalList = it.data
                        }
                    }
                    Resource.Status.ERROR ->
                        Toast.makeText(requireContext(), it.message, Toast.LENGTH_SHORT).show()

                    Resource.Status.LOADING ->
                        binding.progressBar.visibility = View.VISIBLE
                }
            })
    }

    private fun setupRecyclerView() {
        adapter = FarmAnimalAdapter(this)
        binding.farmAnimalRv.adapter = adapter

        MaterialIntroView.Builder(requireActivity())
            .enableDotAnimation(true)
            .enableIcon(false)
            .setFocusGravity(FocusGravity.LEFT)
            .setFocusType(Focus.MINIMUM)
            .setDelayMillis(2000)
            .enableFadeAnimation(true)
            .setInfoText("Swipe left or right to delete.")
            .setShape(ShapeType.CIRCLE)
            .setTarget(view)
            .setIdempotent(true)
            .setUsageId("farm_animal_delete")
            .show()
    }

    override fun onClickFarmAnimal(farmAnimal: FarmAnimal) {
        val action = FarmAnimalListDirections.actionFarmAnimalListToFarmAnimalDetails(farmAnimal.id)
        findNavController().navigate(action)
    }
}