package com.farmhand.farmjoint.ui.health

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import com.farmhand.farmjoint.FarmJoint
import com.farmhand.farmjoint.databinding.FragmentHealthDetailsBinding
import com.farmhand.farmjoint.ui.health.viewModel.UploadHealthRecordViewModel
import com.farmhand.farmjoint.utils.Resource
import com.farmhand.farmjoint.utils.autoCleared
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class HealthDetails : Fragment() {

    private var binding: FragmentHealthDetailsBinding by autoCleared()

    private lateinit var healthId: String
    private lateinit var farmAnimalId: String
    private val viewModel: UploadHealthRecordViewModel by viewModels()


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?
    ): View {
        binding = FragmentHealthDetailsBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        healthId = HealthDetailsArgs.fromBundle(requireArguments()).healthID
        setupObservers()
    }

    private fun setupObservers() {
        viewModel.allRecords("Bearer " + FarmJoint.prefs?.getUserID()!!).observe(viewLifecycleOwner, Observer {
            when (it.status) {
                Resource.Status.SUCCESS -> {
                    binding.progressBar.visibility = View.GONE
                    for (health in it.data!!) {
                        if (health.id == healthId) {
                            binding.date.text = health.date
                            binding.procedureType.text = health.procedure_type
                            binding.procedureDescription.text = health.description
                            binding.vetName.text = health.vet_name
                            farmAnimalId = health.farmanimal_id
                        }
                    }
                }
                Resource.Status.ERROR -> {
                    binding.progressBar.visibility = View.GONE
                    Toast.makeText(requireContext(), it.message, Toast.LENGTH_SHORT).show()
                }

                Resource.Status.LOADING -> {
                    binding.progressBar.visibility = View.VISIBLE
                }
            }
        })

        viewModel.allFarmAnimals("Bearer " + FarmJoint.prefs?.getUserID()!!).observe(viewLifecycleOwner, Observer {
            when (it.status) {
                Resource.Status.SUCCESS -> {
                    binding.progressBar.visibility = View.GONE

                    for (farmAnimal in it.data!!) {
                        if (farmAnimal.id == farmAnimalId) {
                            binding.farmAnimalTag.text = farmAnimal.tag
                            binding.status.text = farmAnimal.status
                        }
                    }
                }
                Resource.Status.ERROR -> {
                    binding.progressBar.visibility = View.GONE

                    Toast.makeText(requireContext(), it.message, Toast.LENGTH_SHORT).show()
                }

                Resource.Status.LOADING -> {
                    binding.progressBar.visibility = View.VISIBLE
                }
            }
        })
    }
}