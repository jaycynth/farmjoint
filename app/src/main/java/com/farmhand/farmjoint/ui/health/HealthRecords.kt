package com.farmhand.farmjoint.ui.health

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import com.farmhand.farmjoint.FarmJoint
import com.farmhand.farmjoint.R
import com.farmhand.farmjoint.adapter.HealthRecordAdapter
import com.farmhand.farmjoint.data.model.HealthRecord
import com.farmhand.farmjoint.databinding.FragmentHealthRecordsBinding
import com.farmhand.farmjoint.ui.health.viewModel.UploadHealthRecordViewModel
import com.farmhand.farmjoint.ui.produce.viewModel.ProduceViewModel
import com.farmhand.farmjoint.utils.Resource
import com.farmhand.farmjoint.utils.autoCleared
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class HealthRecords : Fragment(), HealthRecordAdapter.HealthRecordItemClickListener {

    private var binding: FragmentHealthRecordsBinding by autoCleared()
    private lateinit var adapter: HealthRecordAdapter
    private val viewModel: UploadHealthRecordViewModel by viewModels()



    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?
    ): View {
        binding = FragmentHealthRecordsBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupRecyclerView()
        setupObserver()
        binding.uploadHealthRecord.setOnClickListener { findNavController().navigate(R.id.action_healthRecords_to_uploadHealthRecord) }
    }

    private fun setupObserver() {
        viewModel.allRecords("Bearer " + FarmJoint.prefs?.getUserID()!!).observe(viewLifecycleOwner, Observer {
            when (it.status) {
                Resource.Status.SUCCESS -> {
                    binding.progressBar.visibility = View.GONE
                    if (!it.data.isNullOrEmpty()) adapter.setItems(ArrayList(it.data))
                }
                Resource.Status.ERROR ->
                    Toast.makeText(requireContext(), it.message, Toast.LENGTH_SHORT).show()

                Resource.Status.LOADING ->
                    binding.progressBar.visibility = View.VISIBLE
            }
        })
    }

    private fun setupRecyclerView() {
        adapter = HealthRecordAdapter(this)
        binding.healthRecordRv.adapter = adapter
    }

    override fun onClickHealthRecord(healthRecord: HealthRecord) {
        val action = HealthRecordsDirections.actionHealthRecordsToHealthDetails(healthRecord.id)
        findNavController().navigate(action)    }

}