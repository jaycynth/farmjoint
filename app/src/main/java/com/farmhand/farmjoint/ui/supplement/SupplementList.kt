package com.farmhand.farmjoint.ui.supplement

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import com.farmhand.farmjoint.FarmJoint
import com.farmhand.farmjoint.R
import com.farmhand.farmjoint.adapter.SupplementAdapter
import com.farmhand.farmjoint.data.model.Supplement
import com.farmhand.farmjoint.databinding.FragmentSupplementListBinding
import com.farmhand.farmjoint.ui.supplement.viewModel.SupplementViewModel
import com.farmhand.farmjoint.utils.Resource
import com.farmhand.farmjoint.utils.autoCleared
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class SupplementList : Fragment(), SupplementAdapter.SupplementItemClickListener {

    private var binding: FragmentSupplementListBinding by autoCleared()
    private lateinit var adapter: SupplementAdapter
    private val viewModel: SupplementViewModel by viewModels()


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?
    ): View {
        binding = FragmentSupplementListBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupRecyclerView()
        setupObserver()
        binding.uploadSupplement.setOnClickListener { findNavController().navigate(R.id.action_supplementList_to_addSupplement) }
    }

    private fun setupObserver() {
        viewModel.allSupplements("Bearer " + FarmJoint.prefs?.getUserID()!!).observe(viewLifecycleOwner, Observer {
            when (it.status) {
                Resource.Status.SUCCESS -> {
                    binding.progressBar.visibility = View.GONE
                    if (!it.data.isNullOrEmpty()) adapter.setItems(ArrayList(it.data))
                }
                Resource.Status.ERROR ->
                    Toast.makeText(requireContext(), it.message, Toast.LENGTH_SHORT).show()

                Resource.Status.LOADING ->
                    binding.progressBar.visibility = View.VISIBLE
            }
        })
    }

    private fun setupRecyclerView() {
        adapter = SupplementAdapter(this)
        binding.supplementRv.adapter = adapter
    }

    override fun onClickSupplementItem(supplement: Supplement) {
        val action = SupplementListDirections.actionSupplementListToSupplementDetails(supplement.id)
        findNavController().navigate(action)    }

}