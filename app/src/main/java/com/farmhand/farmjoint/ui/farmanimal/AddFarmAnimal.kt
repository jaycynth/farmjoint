package com.farmhand.farmjoint.ui.farmanimal

import android.app.DatePickerDialog
import android.graphics.Color
import android.os.Bundle
import android.text.TextUtils
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.DatePicker
import android.widget.Toast
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import com.farmhand.farmjoint.FarmJoint
import com.farmhand.farmjoint.R
import com.farmhand.farmjoint.data.model.FarmAnimal
import com.farmhand.farmjoint.databinding.FragmentAddFarmAnimalBinding
import com.farmhand.farmjoint.ui.farmanimal.viewModel.AddFarmAnimalViewModel
import com.farmhand.farmjoint.utils.Resource
import com.farmhand.farmjoint.utils.autoCleared
import com.github.razir.progressbutton.attachTextChangeAnimator
import com.github.razir.progressbutton.bindProgressButton
import com.github.razir.progressbutton.hideProgress
import com.github.razir.progressbutton.showProgress
import com.google.gson.Gson
import com.google.gson.JsonObject
import dagger.hilt.android.AndroidEntryPoint
import okhttp3.MediaType.Companion.toMediaType
import okhttp3.RequestBody.Companion.toRequestBody
import java.text.SimpleDateFormat
import java.util.*

@AndroidEntryPoint
class AddFarmAnimal : Fragment() {
    private var binding: FragmentAddFarmAnimalBinding by autoCleared()
    private val viewModel: AddFarmAnimalViewModel by viewModels()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentAddFarmAnimalBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val addBtn = binding.addFarmAnimalBtn
        bindProgressButton(addBtn)
        addBtn.attachTextChangeAnimator()

        setupStatusDropDown()

        val myCalendar = Calendar.getInstance()
        val date =
            DatePickerDialog.OnDateSetListener { _: DatePicker?, year: Int, monthOfYear: Int, dayOfMonth: Int ->
                myCalendar.set(Calendar.YEAR, year)
                myCalendar.set(Calendar.MONTH, monthOfYear)
                myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth)
                val myFormat = "yyyy-MM-dd"
                val sdf =
                    SimpleDateFormat(myFormat, Locale.getDefault())
                binding.dob.setText(sdf.format(myCalendar.time))
            }

        binding.dob.setOnClickListener {
            DatePickerDialog(
                requireActivity(), date, myCalendar
                    .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                myCalendar.get(Calendar.DAY_OF_MONTH)
            ).show()
        }

        addBtn.setOnClickListener {
            //TODO photo impl
            val tag = binding.farmAnimalTag.text.toString().trim()
            val status = binding.farmAnimalStatus.text.toString().trim()
            val dob = binding.dob.text.toString().trim()
            val noc = binding.noOfCalvings.text.toString().trim()

            when {
                TextUtils.isEmpty(tag) -> {
                    binding.farmAnimalTagLayout.error = "Enter Tag/Name"
                    binding.farmAnimalTag.requestFocus()
                }
                TextUtils.isEmpty(status) -> {
                    Toast.makeText(activity, "Choose status", Toast.LENGTH_SHORT).show()
                    binding.farmAnimalStatus.requestFocus()
                }
                TextUtils.isEmpty(dob) -> {
                    binding.dobLayout.error = "Enter date of birth"
                    binding.dob.requestFocus()
                }
                TextUtils.isEmpty(noc) -> {
                    binding.noOfCalvingsLayout.error = "Enter number of calvings"
                    binding.noOfCalvings.requestFocus()
                }
                else -> {
                    val farmAnimal = FarmAnimal(
                        tag = tag,
                        status = status,
                        photo = "gstk",
                        id = "",
                        birth_date = dob,
                        calfingCount = noc
                    )
                    val json = JsonObject()
                    val jsonElement = Gson().toJsonTree(farmAnimal)
                    json.add("farmanimal", jsonElement)
                    val mediaType = "application/json; charset=utf-8".toMediaType()
                    val requestBody = json.toString().toRequestBody(mediaType)

                    viewModel.addFarmAnimal(requestBody, "Bearer " + FarmJoint.prefs?.getUserID()!!)
                        .observe(viewLifecycleOwner, Observer {
                            when (it.status) {
                                Resource.Status.SUCCESS -> {
                                    addBtn.hideProgress(R.string.done)
                                    Toast.makeText(
                                        activity,
                                        it.data?.success_message,
                                        Toast.LENGTH_LONG
                                    ).show()
                                    binding.farmAnimalTag.setText("")
                                    binding.farmAnimalStatus.setText("")
                                    binding.dob.setText("")
                                    binding.noOfCalvings.setText("")
                                }
                                Resource.Status.ERROR -> {
                                    addBtn.hideProgress(R.string.add_farmanimal_button)
                                    Toast.makeText(requireContext(), it.message, Toast.LENGTH_SHORT)
                                        .show()
                                }

                                Resource.Status.LOADING -> {
                                    addBtn.showProgress {
                                        buttonTextRes = R.string.loading
                                        progressColor = Color.WHITE
                                    }
                                }
                            }
                        })
                }
            }
        }
    }

    private fun setupStatusDropDown() {
        val adapter: ArrayAdapter<String> = ArrayAdapter(
            requireActivity(),
            android.R.layout.simple_dropdown_item_1line,
            getData()
        )
        binding.farmAnimalStatus.setAdapter(adapter)

        binding.statusDropDown.setOnClickListener { binding.farmAnimalStatus.showDropDown() }

        binding.farmAnimalStatus.setOnClickListener { binding.farmAnimalStatus.showDropDown() }
    }

    private fun getData() = mutableListOf(
        "HEALTHY", "SICKLY", "OLD_AGED", "PREGNANT", "EXPECTANT"
    )
}