package com.farmhand.farmjoint.ui.feed.viewModel

import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.ViewModel
import androidx.lifecycle.liveData
import com.farmhand.farmjoint.data.repository.FeedRepository
import com.farmhand.farmjoint.utils.Resource
import kotlinx.coroutines.Dispatchers
import okhttp3.RequestBody

class FeedViewModel @ViewModelInject constructor(
    private val feedRepository: FeedRepository
) : ViewModel() {

    fun allFeeds(accessToken: String) = feedRepository.getAllFeed(accessToken)

    fun addFeed(feed: RequestBody, accessToken: String)  =
        liveData(Dispatchers.IO) {
            emit(Resource.loading())
            val responseStatus = feedRepository.addFeed(
                requestBody = feed, accessToken = accessToken
            )

            if (responseStatus.status == Resource.Status.SUCCESS) {
                emit(Resource.success(responseStatus.data!!))
            } else if (responseStatus.status == Resource.Status.ERROR) {
                emit(Resource.error(responseStatus.message!!))
            }

        }
}