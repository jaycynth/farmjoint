package com.farmhand.farmjoint.ui.feed

import android.app.DatePickerDialog
import android.graphics.Color
import android.os.Bundle
import android.text.TextUtils
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.DatePicker
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import com.farmhand.farmjoint.FarmJoint.Companion.prefs
import com.farmhand.farmjoint.R
import com.farmhand.farmjoint.data.model.Feed
import com.farmhand.farmjoint.databinding.FragmentAddFeedBinding
import com.farmhand.farmjoint.ui.feed.viewModel.FeedViewModel
import com.farmhand.farmjoint.utils.Resource
import com.farmhand.farmjoint.utils.autoCleared
import com.github.razir.progressbutton.attachTextChangeAnimator
import com.github.razir.progressbutton.bindProgressButton
import com.github.razir.progressbutton.hideProgress
import com.github.razir.progressbutton.showProgress
import com.google.gson.Gson
import com.google.gson.JsonObject
import dagger.hilt.android.AndroidEntryPoint
import okhttp3.MediaType.Companion.toMediaType
import okhttp3.RequestBody.Companion.toRequestBody
import java.text.SimpleDateFormat
import java.util.*

@AndroidEntryPoint
class AddFeed : Fragment() {
    private var feedId: String? = null
    private var binding: FragmentAddFeedBinding by autoCleared()
    private val viewModel: FeedViewModel by viewModels()

    private val list: MutableList<String> = ArrayList()

    private var newFeed = false


    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentAddFeedBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding.toggleAddFeed.setOnClickListener {
            if (newFeed) {
                newFeed = false
                binding.feedDropDownLayout.visibility = View.GONE
                binding.feedNameLayout.visibility = View.VISIBLE

            } else {
                newFeed = true
                binding.toggleAddFeed.text = getString(R.string.choose_existing)
                binding.feedDropDownLayout.visibility = View.VISIBLE
                binding.feedNameLayout.visibility = View.GONE
            }

        }

        viewModel.allFeeds("Bearer " + prefs?.getUserID()).observe(viewLifecycleOwner, Observer {
            when (it.status) {
                Resource.Status.SUCCESS -> {
                    if (!it.data.isNullOrEmpty()) {
                        binding.feedDropDownLayout.visibility = View.VISIBLE
                        binding.feedNameLayout.visibility = View.GONE
                        setupFeedDropdown(it)
                    } else {
                        binding.feedDropDownLayout.visibility = View.GONE
                        binding.feedNameLayout.visibility = View.VISIBLE
                    }
                }
                Resource.Status.ERROR ->
                    Toast.makeText(requireContext(), it.message, Toast.LENGTH_SHORT).show()

                Resource.Status.LOADING -> {
                }

            }
        })


        val addBtn = binding.addFeed
        bindProgressButton(addBtn)
        addBtn.attachTextChangeAnimator()
        val myCalendar = Calendar.getInstance()

        val date =
            DatePickerDialog.OnDateSetListener { _: DatePicker?, year: Int, monthOfYear: Int, dayOfMonth: Int ->
                myCalendar.set(Calendar.YEAR, year)
                myCalendar.set(Calendar.MONTH, monthOfYear)
                myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth)
                val myFormat = "yyyy-MM-dd"
                val sdf =
                    SimpleDateFormat(myFormat, Locale.getDefault())
                binding.dateAdded.setText(sdf.format(myCalendar.time))
            }

        binding.dateAdded.setOnClickListener {
            DatePickerDialog(
                requireActivity(), date, myCalendar
                    .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                myCalendar.get(Calendar.DAY_OF_MONTH)
            ).show()
        }

        addBtn.setOnClickListener {
            val dateAdded = binding.dateAdded.text.toString().trim()
            val quantity = binding.feedQuantity.text.toString().trim()
            val price = binding.feedPrice.text.toString().trim()
            val name = binding.feedName.text.toString().trim()
            val dname = binding.feedNameDrop.text.toString().trim()

            when {
                TextUtils.isEmpty(dateAdded) -> {
                    binding.dateLayout.error = "Choose Date"
                    binding.dateAdded.requestFocus()
                }
                TextUtils.isEmpty(name) && TextUtils.isEmpty(dname) -> {
                    binding.feedNameLayout.error = "Add Feed Name"
                    binding.feedName.requestFocus()
                }
                TextUtils.isEmpty(quantity) -> {
                    binding.quantityLayout.error = "Add Feed Quantity"
                    binding.feedQuantity.requestFocus()
                }
                TextUtils.isEmpty(price) -> {
                    binding.feedPriiceLayout.error = "Add Feed Price"
                    binding.feedPrice.requestFocus()
                }
                else -> {
                    if (name != null) {
                        val feed = Feed(
                            date = dateAdded,
                            quantity = quantity,
                            price = price,
                            id = "0",
                            name = name
                        )

                        val json = JsonObject()
                        val jsonElement = Gson().toJsonTree(feed)
                        json.add("feed", jsonElement)
                        val mediaType = "application/json; charset=utf-8".toMediaType()
                        val requestBody = json.toString().toRequestBody(mediaType)

                        viewModel.addFeed(requestBody, "Bearer " + prefs?.getUserID()!!)
                            .observe(viewLifecycleOwner, Observer {
                                when (it.status) {
                                    Resource.Status.SUCCESS -> {
                                        addBtn.hideProgress(R.string.done)
                                        Toast.makeText(
                                            activity,
                                            "feed added successfully",
                                            Toast.LENGTH_LONG
                                        ).show()
                                        binding.dateAdded.setText("")
                                        binding.feedQuantity.setText("")
                                        binding.feedPrice.setText("")
                                        binding.feedName.setText("")
                                    }
                                    Resource.Status.ERROR -> {
                                        addBtn.hideProgress(R.string.add_feed)
                                        Toast.makeText(
                                            requireContext(),
                                            it.message,
                                            Toast.LENGTH_SHORT
                                        )
                                            .show()
                                    }

                                    Resource.Status.LOADING -> {
                                        addBtn.showProgress {
                                            buttonTextRes = R.string.loading
                                            progressColor = Color.WHITE
                                        }
                                    }
                                }
                            })
                    } else {
                        //todo update the feed instead
                    }
                }
            }
        }
    }

    private fun setupFeedDropdown(it: Resource<List<Feed>>) {
        list.clear()
        it.data?.forEach { feed ->
            list.add(feed.name)
        }

        val adapter1: ArrayAdapter<String> = ArrayAdapter(
            requireActivity(),
            android.R.layout.simple_dropdown_item_1line,
            list
        )
        binding.feedNameDrop.setAdapter(adapter1)

        binding.feedDropDown.setOnClickListener { binding.feedNameDrop.showDropDown() }

        binding.feedNameDrop.setOnClickListener { binding.feedNameDrop.showDropDown() }

        binding.feedNameDrop.setOnItemClickListener { parent, _, position, _ ->
            run {
                val feedName = parent.getItemAtPosition(position) as String
                for (feed in it.data!!) {
                    if (feed.name == feedName) {
                        feedId = feed.id
                    }
                }
            }
        }
    }
}