package com.farmhand.farmjoint.ui.supplement.viewModel

import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.ViewModel
import androidx.lifecycle.liveData
import com.farmhand.farmjoint.data.repository.SupplementRepository
import com.farmhand.farmjoint.utils.Resource
import kotlinx.coroutines.Dispatchers
import okhttp3.RequestBody

class SupplementViewModel @ViewModelInject constructor(
    val repository: SupplementRepository
) : ViewModel() {

    fun allSupplements(accessToken: String) = repository.getAllSupplement(accessToken)

    fun addSupplement(supplement: RequestBody, accessToken: String)  =
        liveData(Dispatchers.IO) {
            emit(Resource.loading())
            val responseStatus = repository.addSupplement(
                requestBody = supplement, accessToken = accessToken
            )

            if (responseStatus.status == Resource.Status.SUCCESS) {
                emit(Resource.success(responseStatus.data!!))
            } else if (responseStatus.status == Resource.Status.ERROR) {
                emit(Resource.error(responseStatus.message!!))
            }
        }

}