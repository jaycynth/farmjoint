package com.farmhand.farmjoint.ui.feed

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import com.farmhand.farmjoint.FarmJoint
import com.farmhand.farmjoint.R
import com.farmhand.farmjoint.adapter.FeedAdapter
import com.farmhand.farmjoint.data.model.Feed
import com.farmhand.farmjoint.databinding.FragmentFeedListBinding
import com.farmhand.farmjoint.ui.feed.viewModel.FeedViewModel
import com.farmhand.farmjoint.utils.Resource
import com.farmhand.farmjoint.utils.autoCleared
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class FeedList : Fragment(), FeedAdapter.FeedItemClickListener {
    private var binding: FragmentFeedListBinding by autoCleared()
    private lateinit var adapter: FeedAdapter
    private val viewModel:FeedViewModel by viewModels()


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?
    ): View{
        binding = FragmentFeedListBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupRecyclerView()
        setupObserver()
        binding.uploadNewFeed.setOnClickListener { findNavController().navigate(R.id.action_feedList_to_addFeed) }
    }

    private fun setupObserver() {
        viewModel.allFeeds("Bearer " + FarmJoint.prefs?.getUserID()!!).observe(viewLifecycleOwner, Observer {
            when (it.status) {
                Resource.Status.SUCCESS -> {
                    binding.progressBar.visibility = View.GONE
                    if (!it.data.isNullOrEmpty()) adapter.setItems(ArrayList(it.data))
                }
                Resource.Status.ERROR ->
                    Toast.makeText(requireContext(), it.message, Toast.LENGTH_SHORT).show()

                Resource.Status.LOADING ->
                    binding.progressBar.visibility = View.VISIBLE
            }
        })
    }

    private fun setupRecyclerView() {
        adapter = FeedAdapter(this)
        binding.feedRv.adapter = adapter
    }

    override fun onClickFeed(feed: Feed) {
        val action = FeedListDirections.actionFeedListToFeedDetails(feed.id)
        findNavController().navigate(action)
    }

}

