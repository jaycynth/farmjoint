package com.farmhand.farmjoint.ui.produce


import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import com.farmhand.farmjoint.FarmJoint.Companion.prefs
import com.farmhand.farmjoint.R
import com.farmhand.farmjoint.databinding.FragmentProduceListBinding
import com.farmhand.farmjoint.adapter.ProduceAdapter
import com.farmhand.farmjoint.data.model.Produce
import com.farmhand.farmjoint.ui.produce.viewModel.ProduceViewModel
import com.farmhand.farmjoint.utils.Resource
import com.farmhand.farmjoint.utils.autoCleared
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class ProduceList : Fragment() , ProduceAdapter.ProduceItemListener{

    private var binding: FragmentProduceListBinding by autoCleared()
    private lateinit var adapter: ProduceAdapter
    private val viewModel: ProduceViewModel by viewModels()


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?
    ): View {
        binding = FragmentProduceListBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupRecyclerView()
        setupObservers()
        binding.addProduce.setOnClickListener { findNavController().navigate(R.id.action_produceList_to_addProduce) }
    }

    private fun setupObservers() {
        viewModel.allProduce("Bearer " + prefs?.getUserID()!!).observe(viewLifecycleOwner, Observer {
            when (it.status) {
                Resource.Status.SUCCESS -> {
                    binding.progressBar.visibility = View.GONE
                    if (!it.data.isNullOrEmpty()) adapter.setItems(ArrayList(it.data))
                }
                Resource.Status.ERROR ->
                    Toast.makeText(requireContext(), it.message, Toast.LENGTH_SHORT).show()

                Resource.Status.LOADING ->
                    binding.progressBar.visibility = View.VISIBLE
            }
        })    }

    private fun setupRecyclerView() {
        adapter = ProduceAdapter(this)
        binding.produceRv.adapter = adapter
    }

    override fun onClickedProduce(produce: Produce) {
        val action = ProduceListDirections.actionProduceListToProduceDetails(produce.id)
        findNavController().navigate(action)
    }

}