package com.farmhand.farmjoint.ui.home

import android.os.Bundle

import android.view.View
import androidx.fragment.app.Fragment
import androidx.navigation.NavController
import androidx.navigation.Navigation
import androidx.navigation.fragment.findNavController
import com.farmhand.farmjoint.FarmJoint.Companion.prefs
import com.farmhand.farmjoint.R
import com.farmhand.farmjoint.adapter.CategoryAdapter
import com.farmhand.farmjoint.data.model.Category
import com.farmhand.farmjoint.data.model.getData
import com.farmhand.farmjoint.databinding.FragmentHomeBinding
import dagger.hilt.android.AndroidEntryPoint
import org.imaginativeworld.whynotimagecarousel.CarouselItem

@AndroidEntryPoint
class HomeFragment : Fragment(R.layout.fragment_home), CategoryAdapter.CategoryItemClickListener {
    private lateinit var fragmentHomeBinding: FragmentHomeBinding
    private lateinit var sliderNamesArray: Array<String>
    private lateinit var navController: NavController
    private var currentPage = 0

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        fragmentHomeBinding = FragmentHomeBinding.bind(view)
        navController = Navigation.findNavController(view)

        val accessToken = prefs?.getUserID()

        if (accessToken == "null") {
            navController.navigate(R.id.action_homeFragment_to_loginFragment)
        }

        fragmentHomeBinding.username.text = prefs?.getUserName()

        fragmentHomeBinding.viewMore.setOnClickListener {
            navController.navigate(R.id.updateFragment)
        }

        val list = mutableListOf<CarouselItem>()

        list.add(
            CarouselItem(
                imageUrl = "https://images.unsplash.com/photo-1464226184884-fa280b87c399?ixlib=rb-1.2.1&ixid=MXwxMjA3fDB8MHxzZWFyY2h8MTN8fGFncmljdWx0dXJlfGVufDB8fDB8&auto=format&fit=crop&w=1000&q=60",
                caption = "Crop rotation in rural areas"
            )
        )
        list.add(
            CarouselItem(
                imageUrl = "https://images.unsplash.com/photo-1557234195-bd9f290f0e4d?ixlib=rb-1.2.1&ixid=MXwxMjA3fDB8MHxzZWFyY2h8Mnx8YWdyaWN1bHR1cmV8ZW58MHx8MHw%3D&auto=format&fit=crop&w=1000&q=60",
                caption = "AgriTech"
            )
        )
        list.add(
            CarouselItem(
                imageUrl = "https://images.unsplash.com/photo-1500382017468-9049fed747ef?ixlib=rb-1.2.1&ixid=MXwxMjA3fDB8MHxzZWFyY2h8Mjh8fGFncmljdWx0dXJlfGVufDB8fDB8&auto=format&fit=crop&w=1000&q=60",
                caption = "Agriculture Event "
            )
        )

        fragmentHomeBinding.carousel.addData(list)

        categoriesRvSetup()

        fragmentHomeBinding.logout.setOnClickListener {
            findNavController().navigate(R.id.loginFragment)
        }
    }

    private fun categoriesRvSetup() {
        val categories = getData()
        val categoryAdapter = CategoryAdapter(categories, this)
        with(fragmentHomeBinding.categoryRv) {
            adapter = categoryAdapter
        }
    }


    override fun onCategoryItemClicked(category: Category) {
        when (category.name) {
            "Farm Animals" -> {
                navController.navigate(R.id.action_homeFragment_to_farmAnimalList)
            }
            "Produce Records" -> {
                navController.navigate(R.id.action_homeFragment_to_produceList)
            }
            "Feed Records" -> {
                navController.navigate(R.id.action_homeFragment_to_feedList)
            }
            "Health Records" -> {
                navController.navigate(R.id.action_homeFragment_to_healthRecords)
            }
            "Supplement Records" -> {
                navController.navigate(R.id.action_homeFragment_to_supplementList)
            }
        }
    }
}





