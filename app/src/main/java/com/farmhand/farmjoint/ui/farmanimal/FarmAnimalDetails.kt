package com.farmhand.farmjoint.ui.farmanimal

import android.annotation.SuppressLint
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import com.farmhand.farmjoint.FarmJoint.Companion.prefs
import com.farmhand.farmjoint.R
import com.farmhand.farmjoint.adapter.ViewProduceAdapter
import com.farmhand.farmjoint.data.model.Produce
import com.farmhand.farmjoint.databinding.FragmentFarmAnimalDetailsBinding
import com.farmhand.farmjoint.ui.farmanimal.viewModel.FarmAnimalViewModel
import com.farmhand.farmjoint.utils.Resource
import com.farmhand.farmjoint.utils.autoCleared
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class FarmAnimalDetails : Fragment() {
    private var binding: FragmentFarmAnimalDetailsBinding by autoCleared()

    private val viewModel: FarmAnimalViewModel by viewModels()

    private lateinit var farmAnimalId: String

    private var produceOpened = false

    private lateinit var adapter: ViewProduceAdapter


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?
    ): View {
        binding = FragmentFarmAnimalDetailsBinding.inflate(inflater, container, false)
        setupObservers()
        return binding.root
    }

    @SuppressLint("SetTextI18n")
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        farmAnimalId = FarmAnimalDetailsArgs.fromBundle(requireArguments()).farmAnimalID

        binding.viewItsProduce.setOnClickListener {

            if (produceOpened) {
                produceOpened = false
                binding.produceLayout.visibility = View.GONE

            } else {
                produceOpened = true
                binding.produceLayout.visibility = View.VISIBLE

                setupRecyclerView()

                viewModel.getProduceListForFarmanimal(
                    "Bearer " + prefs?.getUserID()!!,
                    farmAnimalId
                ).observe(viewLifecycleOwner, Observer {
                    when (it.status) {
                        Resource.Status.SUCCESS -> {
                            binding.progressBarProduce.visibility = View.GONE
                            if (!it.data?.produce_list.isNullOrEmpty()) {
                                adapter.setItems((it.data?.produce_list as ArrayList<Produce>?)!!)
                            }
                        }
                        Resource.Status.ERROR -> {
                            Toast.makeText(requireContext(), it.message, Toast.LENGTH_SHORT).show()
                        }

                        Resource.Status.LOADING -> {
                            binding.progressBarProduce.visibility = View.VISIBLE
                        }
                    }
                })
            }
        }
    }

    private fun setupRecyclerView() {
        adapter = ViewProduceAdapter()
        binding.viewProduceRv.adapter = adapter
    }

    private fun setupObservers() {

        viewModel.allFarmAnimals("Bearer " + prefs?.getUserID()!!)
            .observe(viewLifecycleOwner, Observer {
                when (it.status) {
                    Resource.Status.SUCCESS -> {
                        binding.baseLayout.visibility = View.VISIBLE
                        binding.progressBar.visibility = View.GONE
                        for (farmAnimal in it.data!!) {
                            if (farmAnimal.id == farmAnimalId) {
                                binding.farmAnimalTag.text = farmAnimal.tag
                                binding.status.text = farmAnimal.status
                            }
                        }
                    }
                    Resource.Status.ERROR -> {
                        binding.baseLayout.visibility = View.VISIBLE
                        binding.progressBar.visibility = View.GONE
                        Toast.makeText(requireContext(), it.message, Toast.LENGTH_SHORT).show()
                    }

                    Resource.Status.LOADING -> {
                        binding.baseLayout.visibility = View.GONE
                        binding.progressBar.visibility = View.VISIBLE
                    }
                }
            })

    }
}