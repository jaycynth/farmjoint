package com.farmhand.farmjoint.ui.health

import android.app.DatePickerDialog
import android.graphics.Color
import android.os.Bundle
import android.text.TextUtils
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.AutoCompleteTextView
import android.widget.DatePicker
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import com.farmhand.farmjoint.FarmJoint
import com.farmhand.farmjoint.R
import com.farmhand.farmjoint.data.model.FarmAnimal
import com.farmhand.farmjoint.data.model.HealthRecord
import com.farmhand.farmjoint.databinding.FragmentUploadHealthRecordBinding
import com.farmhand.farmjoint.ui.health.viewModel.UploadHealthRecordViewModel
import com.farmhand.farmjoint.utils.Resource
import com.farmhand.farmjoint.utils.autoCleared
import com.github.razir.progressbutton.attachTextChangeAnimator
import com.github.razir.progressbutton.bindProgressButton
import com.github.razir.progressbutton.hideProgress
import com.github.razir.progressbutton.showProgress
import com.google.gson.Gson
import com.google.gson.JsonObject
import dagger.hilt.android.AndroidEntryPoint
import okhttp3.MediaType.Companion.toMediaType
import okhttp3.RequestBody.Companion.toRequestBody
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList

@AndroidEntryPoint
class UploadHealthRecord : Fragment() {
    private var binding: FragmentUploadHealthRecordBinding by autoCleared()
    private val viewModel: UploadHealthRecordViewModel by viewModels()

    private val farmAnimalList: MutableList<FarmAnimal> = ArrayList()

    private var farmAnimalId = "0"

    val list: MutableList<String> = ArrayList()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentUploadHealthRecordBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val farmAnimalTv = binding.farmAnimal
        bindProgressButton(farmAnimalTv)
        farmAnimalTv.attachTextChangeAnimator()

        setupObserver(farmAnimalTv)

        val addBtn = binding.uploadHealthRecord
        bindProgressButton(addBtn)
        addBtn.attachTextChangeAnimator()
        val myCalendar = Calendar.getInstance()

        val date =
            DatePickerDialog.OnDateSetListener { _: DatePicker?, year: Int, monthOfYear: Int, dayOfMonth: Int ->
                myCalendar.set(Calendar.YEAR, year)
                myCalendar.set(Calendar.MONTH, monthOfYear)
                myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth)
                val myFormat = "yyyy-MM-dd"
                val sdf =
                    SimpleDateFormat(myFormat, Locale.getDefault())
                binding.dateAdded.setText(sdf.format(myCalendar.time))
            }

        binding.dateAdded.setOnClickListener {
            DatePickerDialog(
                requireActivity(), date, myCalendar
                    .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                myCalendar.get(Calendar.DAY_OF_MONTH)
            ).show()
        }

        addBtn.setOnClickListener {
            val dateAdded = binding.dateAdded.text.toString().trim()
            val procedureType = binding.procedureType.text.toString().trim()
            val vetName = binding.vetName.text.toString().trim()
            val farmanimal = binding.farmAnimal.text.toString().trim()
            val desc = binding.healthDescription.text.toString().trim()


            when {
                TextUtils.isEmpty(dateAdded) -> {
                    binding.dateLayout.error = "Choose Date"
                    binding.dateAdded.requestFocus()
                }
                TextUtils.isEmpty(procedureType) -> {
                    binding.procedureTypeLayout.error = "Add Procedure Type"
                    binding.procedureType.requestFocus()
                }
                TextUtils.isEmpty(vetName) -> {
                    binding.vetNameLayout.error = "Add Vet Name"
                    binding.vetName.requestFocus()
                }
                TextUtils.isEmpty(farmanimal) -> {
                    Toast.makeText(requireActivity(), "Choose farm animal", Toast.LENGTH_SHORT)
                        .show()
                    binding.farmAnimal.requestFocus()
                }
                TextUtils.isEmpty(desc) -> {
                    binding.descriptionLayout.error = "Add Description"
                    binding.healthDescription.requestFocus()
                }
                else -> {
                    val health = HealthRecord(
                        date = dateAdded,
                        description = desc,
                        procedure_type = procedureType,
                        vet_name = vetName,
                        farmanimal_id = farmAnimalId,
                        id = "0"
                    )

                    val json = JsonObject()
                    val jsonElement = Gson().toJsonTree(health)
                    json.add("health", jsonElement)
                    val mediaType = "application/json; charset=utf-8".toMediaType()
                    val requestBody = json.toString().toRequestBody(mediaType)

                    viewModel.addHealthRecord(
                        requestBody,
                        "Bearer " + FarmJoint.prefs?.getUserID()!!
                    )
                        .observe(viewLifecycleOwner, Observer {
                            when (it.status) {
                                Resource.Status.SUCCESS -> {
                                    addBtn.hideProgress(R.string.done)
                                    Toast.makeText(
                                        activity,
                                        "Health Record added successfully",
                                        Toast.LENGTH_LONG
                                    ).show()
                                    binding.dateAdded.setText("")
                                    binding.procedureType.setText("")
                                    binding.vetName.setText("")
                                    binding.farmAnimal.setText("")
                                    binding.healthDescription.setText("")


                                }
                                Resource.Status.ERROR -> {
                                    addBtn.hideProgress(R.string.upload)
                                    Toast.makeText(requireContext(), it.message, Toast.LENGTH_SHORT)
                                        .show()
                                }

                                Resource.Status.LOADING -> {
                                    addBtn.showProgress {
                                        buttonTextRes = R.string.loading
                                        progressColor = Color.WHITE
                                    }
                                }
                            }
                        })
                }
            }
        }
    }

    private fun setupObserver(farmAnimalTv: AutoCompleteTextView) {
        viewModel.allFarmAnimals("Bearer " + FarmJoint.prefs?.getUserID()!!)
            .observe(viewLifecycleOwner, Observer {
                when (it.status) {
                    Resource.Status.SUCCESS -> {
                        farmAnimalTv.hideProgress("")
                        //todo called twice issue
                        it.data?.let { it1 -> farmAnimalList.addAll(it1) }
                        list.clear()
                        it.data?.forEach { farmAnimal ->
                            list.add(farmAnimal.tag)
                        }
                        setupFarmAnimalDropDown(farmAnimalTv)


                    }
                    Resource.Status.ERROR -> {
                        farmAnimalTv.hideProgress("")
                        Toast.makeText(requireContext(), it.message, Toast.LENGTH_SHORT).show()
                    }

                    Resource.Status.LOADING -> {
                        farmAnimalTv.showProgress {
                            buttonTextRes = R.string.loading
                            progressColor = Color.BLACK
                        }
                    }
                }
            })
    }

    private fun setupFarmAnimalDropDown(farmAnimalTv: AutoCompleteTextView) {
        val adapter1: ArrayAdapter<String> = ArrayAdapter(
            requireActivity(),
            android.R.layout.simple_dropdown_item_1line,
            list
        )
        farmAnimalTv.setAdapter(adapter1)

        binding.faDropDown.setOnClickListener { farmAnimalTv.showDropDown() }

        farmAnimalTv.setOnClickListener { farmAnimalTv.showDropDown() }

        farmAnimalTv.setOnItemClickListener { parent, _, position, _ ->
            run {
                val farmAnimalTag = parent.getItemAtPosition(position) as String
                for (farmAnimal in farmAnimalList) {
                    if (farmAnimal.tag == farmAnimalTag) {
                        farmAnimalId = farmAnimal.id
                    }
                }
            }
        }
    }
}