package com.farmhand.farmjoint.ui.farmanimal.viewModel

import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.ViewModel
import androidx.lifecycle.liveData
import com.farmhand.farmjoint.data.repository.FarmAnimalRepository
import com.farmhand.farmjoint.data.repository.ProduceRepository
import com.farmhand.farmjoint.utils.Resource
import kotlinx.coroutines.Dispatchers

class FarmAnimalViewModel @ViewModelInject constructor(
    val farmAnimalRepo: FarmAnimalRepository,
    private val produceRepo:ProduceRepository
) : ViewModel() {

    fun allFarmAnimals(accessToken:String) = farmAnimalRepo.getAllFarmAnimals(accessToken = accessToken)
     fun allProduce(accessToken: String) = produceRepo.getAllProduce(accessToken)

    fun getProduceListForFarmanimal(accessToken: String, farmanimal_id: String) =
        liveData(Dispatchers.IO) {
            emit(Resource.loading())
            val responseStatus = produceRepo.getProduceListForFarmAnimal(
                accessToken = accessToken, farmanimalId = farmanimal_id
            )

            if (responseStatus.status == Resource.Status.SUCCESS) {
                emit(Resource.success(responseStatus.data!!))
            } else if (responseStatus.status == Resource.Status.ERROR) {
                emit(Resource.error(responseStatus.message!!))
            }
        }

    fun  deleteFarmAnimal(accessToken: String, farmanimal_id: String) =
        liveData(Dispatchers.IO) {
            emit(Resource.loading())
            val responseStatus = farmAnimalRepo.deleteFarmAnimal(
                accessToken = accessToken, farmAnimalId = farmanimal_id
            )

            if (responseStatus.status == Resource.Status.SUCCESS) {
                emit(Resource.success(responseStatus.data!!))
            } else if (responseStatus.status == Resource.Status.ERROR) {
                emit(Resource.error(responseStatus.message!!))
            }
        }

}