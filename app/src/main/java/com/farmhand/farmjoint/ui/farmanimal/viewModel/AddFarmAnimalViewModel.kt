package com.farmhand.farmjoint.ui.farmanimal.viewModel

import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.ViewModel
import androidx.lifecycle.liveData
import com.farmhand.farmjoint.data.repository.FarmAnimalRepository
import com.farmhand.farmjoint.utils.Resource
import kotlinx.coroutines.Dispatchers
import okhttp3.RequestBody


class AddFarmAnimalViewModel @ViewModelInject constructor(
    private val repository: FarmAnimalRepository
) : ViewModel() {


    fun addFarmAnimal(farmAnimal: RequestBody, accessToken:String) =
        liveData(Dispatchers.IO) {
            emit(Resource.loading())
            val responseStatus = repository.addFarmAnimal(
                requestBody = farmAnimal,
                accessToken = accessToken
            )

            if (responseStatus.status == Resource.Status.SUCCESS) {
                emit(Resource.success(responseStatus.data!!))
            } else if (responseStatus.status == Resource.Status.ERROR) {
                emit(Resource.error(responseStatus.message!!))
            }

        }
}
