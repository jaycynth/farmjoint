package com.farmhand.farmjoint.ui.login.viewModel

import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.ViewModel
import androidx.lifecycle.liveData
import com.farmhand.farmjoint.data.repository.UserRepository
import com.farmhand.farmjoint.utils.Resource
import kotlinx.coroutines.Dispatchers
import okhttp3.RequestBody

class LoginViewModel @ViewModelInject constructor(private val repository: UserRepository) :
    ViewModel() {
    fun login(requestBody: RequestBody) =
        liveData(Dispatchers.IO) {
            emit(Resource.loading())
            val responseStatus = repository.login(
                requestBody = requestBody
            )

            if (responseStatus.status == Resource.Status.SUCCESS) {
                emit(Resource.success(responseStatus.data!!))
            } else if (responseStatus.status == Resource.Status.ERROR) {
                emit(Resource.error(responseStatus.message!!))
            }

        }
}