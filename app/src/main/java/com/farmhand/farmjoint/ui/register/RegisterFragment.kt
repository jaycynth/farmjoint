package com.farmhand.farmjoint.ui.register

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.observe
import androidx.navigation.fragment.findNavController
import com.farmhand.farmjoint.R
import com.farmhand.farmjoint.data.model.User
import com.farmhand.farmjoint.databinding.FragmentRegisterBinding
import com.farmhand.farmjoint.ui.register.viewModel.RegisterViewModel
import com.farmhand.farmjoint.utils.Resource
import com.google.gson.Gson
import com.google.gson.JsonObject
import dagger.hilt.android.AndroidEntryPoint
import okhttp3.MediaType.Companion.toMediaType
import okhttp3.RequestBody.Companion.toRequestBody

@AndroidEntryPoint
class RegisterFragment : Fragment() {

    private lateinit var binding: FragmentRegisterBinding
    private val viewModel: RegisterViewModel by viewModels()


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?
    ): View {
        binding = FragmentRegisterBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding.registerBtn.setOnClickListener {
            val username = binding.username.text.toString().trim()
            val phonenumber = binding.phoneNumber.text.toString().trim()

            when {
                username.isEmpty() -> {
                    binding.usernameLayout.error = "Required field"
                    binding.username.requestFocus()
                }
                phonenumber.isEmpty() -> {
                    binding.phoneNumberLayout.error = "Required field"
                    binding.phoneNumber.requestFocus()
                }
                else -> {
                    val user = User(
                        user_name = username,
                        phone_number = phonenumber
                    )

                    val json = JsonObject()
                    val jsonElement = Gson().toJsonTree(user)
                    json.add("user", jsonElement)
                    val mediaType = "application/json; charset=utf-8".toMediaType()
                    val requestBody = json.toString().toRequestBody(mediaType)

                    viewModel.register(requestBody).observe(viewLifecycleOwner) {
                        when (it.status) {
                            Resource.Status.SUCCESS -> {
                                Toast.makeText(
                                    activity,
                                    "Account Created; Login to Proceed",
                                    Toast.LENGTH_LONG
                                ).show()

                                findNavController().navigate(R.id.loginFragment)

                            }
                            Resource.Status.ERROR -> {
                                Toast.makeText(
                                    requireContext(),
                                    it.message,
                                    Toast.LENGTH_SHORT
                                )
                                    .show()
                            }
                            Resource.Status.LOADING -> {
                                Toast.makeText(
                                    activity,
                                    "Loading ....",
                                    Toast.LENGTH_LONG
                                ).show()
                            }
                        }
                    }
                }
            }
        }
    }
}