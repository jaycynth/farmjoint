package com.farmhand.farmjoint.ui.feed

import android.annotation.SuppressLint
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import com.farmhand.farmjoint.FarmJoint
import com.farmhand.farmjoint.databinding.FragmentFeedDetailsBinding
import com.farmhand.farmjoint.ui.feed.viewModel.FeedViewModel
import com.farmhand.farmjoint.utils.Resource
import com.farmhand.farmjoint.utils.autoCleared
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class FeedDetails : Fragment() {
    private var binding: FragmentFeedDetailsBinding by autoCleared()

    private lateinit var feedId :String
    private val viewModel: FeedViewModel by viewModels()


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?
    ): View {
        binding = FragmentFeedDetailsBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        feedId = FeedDetailsArgs.fromBundle(requireArguments()).feedID
        setupObservers()
    }

    @SuppressLint("SetTextI18n")
    private fun setupObservers() {
        viewModel.allFeeds("Bearer " + FarmJoint.prefs?.getUserID()!!).observe(viewLifecycleOwner, Observer {
            when (it.status) {
                Resource.Status.SUCCESS -> {
                    binding.progressBar.visibility = View.GONE
                    for (feed in it.data!!) {
                        if (feed.id == feedId) {
                            binding.feedName.text = "# ${feed.id}"
                            binding.date.text = feed.date
                            binding.quantity.text = feed.quantity
                            binding.price.text = "Ksh ${feed.price}"
                        }
                    }
                }
                Resource.Status.ERROR -> {
                    binding.progressBar.visibility = View.GONE
                    Toast.makeText(requireContext(), it.message, Toast.LENGTH_SHORT).show()
                }

                Resource.Status.LOADING -> {
                    binding.progressBar.visibility = View.VISIBLE
                }
            }
        })    }
}