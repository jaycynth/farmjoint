package com.farmhand.farmjoint.ui.produce.viewModel

import com.farmhand.farmjoint.data.repository.ProduceRepository
import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.*
import com.farmhand.farmjoint.data.repository.FarmAnimalRepository


class ProduceViewModel @ViewModelInject constructor(
    val repository: ProduceRepository, val farmAnimalRepo: FarmAnimalRepository
) : ViewModel() {

    fun allProduce(accessToken: String) = repository.getAllProduce(accessToken)

    fun allFarmAnimals(accessToken: String) = farmAnimalRepo.getAllFarmAnimals(accessToken)

}
