package com.farmhand.farmjoint.ui.supplement

import android.annotation.SuppressLint
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import com.farmhand.farmjoint.FarmJoint
import com.farmhand.farmjoint.databinding.FragmentSupplementDetailsBinding
import com.farmhand.farmjoint.ui.supplement.viewModel.SupplementViewModel
import com.farmhand.farmjoint.utils.Resource
import com.farmhand.farmjoint.utils.autoCleared
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class SupplementDetails : Fragment() {
    private var binding: FragmentSupplementDetailsBinding by autoCleared()
    private lateinit var supplementId :String
    private val viewModel: SupplementViewModel by viewModels()


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?
    ): View {
        binding = FragmentSupplementDetailsBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        supplementId = SupplementDetailsArgs.fromBundle(requireArguments()).supplementID
        setupObserver()
    }

    @SuppressLint("SetTextI18n")
    private fun setupObserver() {
        viewModel.allSupplements("Bearer " + FarmJoint.prefs?.getUserID()!!).observe(viewLifecycleOwner, Observer {
            when (it.status) {
                Resource.Status.SUCCESS -> {
                    binding.progressBar.visibility = View.GONE
                    for (supplement in it.data!!) {
                        if (supplement.id == supplementId) {
                            binding.name.text = supplement.supplement_name
                            binding.units.text = supplement.units
                            binding.quantity.text = supplement.quantity
                            binding.price.text = "Ksh ${supplement.price}"
                        }
                    }
                }
                Resource.Status.ERROR -> {
                    binding.progressBar.visibility = View.GONE
                    Toast.makeText(requireContext(), it.message, Toast.LENGTH_SHORT).show()
                }

                Resource.Status.LOADING -> {
                    binding.progressBar.visibility = View.VISIBLE
                }
            }
        })
    }
}