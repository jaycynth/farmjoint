package com.farmhand.farmjoint.ui.produce

import androidx.fragment.app.Fragment
import com.farmhand.farmjoint.R
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class ProducePurchase : Fragment(R.layout.fragment_produce_purchase) {

}