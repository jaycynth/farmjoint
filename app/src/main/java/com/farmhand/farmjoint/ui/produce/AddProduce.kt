package com.farmhand.farmjoint.ui.produce

import android.app.DatePickerDialog
import android.app.DatePickerDialog.OnDateSetListener
import android.graphics.Color
import android.os.Bundle
import android.text.TextUtils
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.AutoCompleteTextView
import android.widget.DatePicker
import android.widget.Toast
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import com.farmhand.farmjoint.FarmJoint.Companion.prefs
import com.farmhand.farmjoint.R
import com.farmhand.farmjoint.data.model.FarmAnimal
import com.farmhand.farmjoint.data.model.Produce
import com.farmhand.farmjoint.databinding.FragmentAddProduceBinding
import com.farmhand.farmjoint.ui.produce.viewModel.AddProduceViewModel
import com.farmhand.farmjoint.utils.Resource
import com.farmhand.farmjoint.utils.autoCleared
import com.github.razir.progressbutton.*
import com.google.gson.Gson
import com.google.gson.JsonObject
import dagger.hilt.android.AndroidEntryPoint
import okhttp3.MediaType.Companion.toMediaType
import okhttp3.RequestBody.Companion.toRequestBody
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList

@AndroidEntryPoint
class AddProduce : androidx.fragment.app.Fragment() {
    private var binding: FragmentAddProduceBinding by autoCleared()
    private val viewModel: AddProduceViewModel by viewModels()
    private val farmAnimalList: MutableList<FarmAnimal> = ArrayList()

    private var farmAnimalId = "0"

    val list: MutableList<String> = ArrayList()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentAddProduceBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val addBtn = binding.add
        bindProgressButton(addBtn)
        addBtn.attachTextChangeAnimator()

        val farmAnimalTv = binding.farmAnimal
        bindProgressButton(farmAnimalTv)
        farmAnimalTv.attachTextChangeAnimator()

        setupObserver(farmAnimalTv)

        setupQuantityTypeDropDown()

        val myCalendar = Calendar.getInstance()
        val date =
            OnDateSetListener { _: DatePicker?, year: Int, monthOfYear: Int, dayOfMonth: Int ->
                myCalendar.set(Calendar.YEAR, year)
                myCalendar.set(Calendar.MONTH, monthOfYear)
                myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth)
                val myFormat = "yyyy-MM-dd"
                val sdf =
                    SimpleDateFormat(myFormat, Locale.getDefault())
                binding.dateAdded.setText(sdf.format(myCalendar.time))
            }

        binding.dateAdded.setOnClickListener {
            DatePickerDialog(
                requireActivity(), date, myCalendar
                    .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                myCalendar.get(Calendar.DAY_OF_MONTH)
            ).show()
        }

        addBtn.setOnClickListener {
            val prodType = binding.produceType.text.toString().trim()
            val prodQuantity = binding.quantity.text.toString().trim()
            val prodDate = binding.dateAdded.text.toString().trim()
            val prodUnitPrice = binding.unitPrice.text.toString().trim()
            val farmAnimal = farmAnimalTv.text.toString().trim()
            val quantityType = binding.quantityType.text.toString().trim()

            when {
                TextUtils.isEmpty(prodType) -> {
                    binding.produceTypeLayout.error = "Enter Produce Type"
                    binding.produceType.requestFocus()
                }
                TextUtils.isEmpty(prodQuantity) -> {
                    binding.quantityLayout.error = "Enter Quantity"
                    binding.quantity.requestFocus()
                }
                TextUtils.isEmpty(farmAnimal) -> {
                    Toast.makeText(activity, "Choose Farm Annimal", Toast.LENGTH_SHORT).show()
                }
                TextUtils.isEmpty(prodDate) -> {
                    binding.dateLayout.error = "Choose Date"
                    binding.dateAdded.requestFocus()
                }
                TextUtils.isEmpty(prodUnitPrice) -> {
                    binding.unitPriceLayout.error = "Add Unit Price For Produce"
                    binding.unitPrice.requestFocus()
                }
                TextUtils.isEmpty(quantityType) -> {
                    Toast.makeText(activity, "Choose a quantity type", Toast.LENGTH_SHORT).show()
                }
                else -> {

                    val produce = Produce(
                        date = prodDate,
                        each_price = prodUnitPrice.toInt(),
                        produce_type = prodType,
                        quantity = prodQuantity.toInt(),
                        farmanimal_id = farmAnimalId,
                        id = "",
                        total_price = 0,
                        quantity_type = quantityType
                    )
                    val json = JsonObject()
                    val jsonElement = Gson().toJsonTree(produce)
                    json.add("produce", jsonElement)
                    val mediaType = "application/json; charset=utf-8".toMediaType()
                    val requestBody = json.toString().toRequestBody(mediaType)

                    viewModel.addProduce(requestBody, "Bearer " + prefs?.getUserID()!!)
                        .observe(viewLifecycleOwner, Observer {
                            when (it.status) {
                                Resource.Status.SUCCESS -> {
                                    addBtn.hideProgress(R.string.done)
                                    Toast.makeText(
                                        activity,
                                        "produce added successfully",
                                        Toast.LENGTH_LONG
                                    ).show()

                                    binding.produceType.setText("")
                                    binding.quantity.setText("")
                                    binding.dateAdded.setText("")
                                    binding.unitPrice.setText("")
                                    farmAnimalTv.setText("")
                                    binding.quantityType.setText("")
                                }
                                Resource.Status.ERROR -> {
                                    addBtn.hideProgress(R.string.add_produce_button)
                                    Toast.makeText(requireContext(), it.message, Toast.LENGTH_SHORT)
                                        .show()
                                }

                                Resource.Status.LOADING -> {
                                    addBtn.showProgress {
                                        buttonTextRes = R.string.loading
                                        progressColor = Color.WHITE
                                    }
                                }
                            }
                        })
                }
            }
        }
    }

    private fun setupObserver(farmAnimalTv: AutoCompleteTextView) {
        viewModel.allFarmAnimals("Bearer " + prefs?.getUserID())
            .observe(viewLifecycleOwner, Observer {
                when (it.status) {
                    Resource.Status.SUCCESS -> {
                        farmAnimalTv.hideProgress("")
                        //todo called twice issue
                        it.data?.let { it1 -> farmAnimalList.addAll(it1) }
                        list.clear()
                        it.data?.forEach { farmAnimal ->
                            list.add(farmAnimal.tag)
                        }
                        setupFarmAnimalDropDown(farmAnimalTv)


                    }
                    Resource.Status.ERROR -> {
                        farmAnimalTv.hideProgress("")
                        Toast.makeText(requireContext(), it.message, Toast.LENGTH_SHORT).show()
                    }

                    Resource.Status.LOADING -> {
                        farmAnimalTv.showProgress {
                            buttonTextRes = R.string.loading
                            progressColor = Color.BLACK
                        }
                    }
                }
            })

    }

    private fun setupFarmAnimalDropDown(farmAnimalTv: AutoCompleteTextView) {

        val adapter1: ArrayAdapter<String> = ArrayAdapter(
            requireActivity(),
            android.R.layout.simple_dropdown_item_1line,
            list
        )
        farmAnimalTv.setAdapter(adapter1)

        binding.faDropDown.setOnClickListener { farmAnimalTv.showDropDown() }

        farmAnimalTv.setOnClickListener { farmAnimalTv.showDropDown() }

        farmAnimalTv.setOnItemClickListener { parent, _, position, _ ->
            run {
                val farmAnimalTag = parent.getItemAtPosition(position) as String
                for (farmAnimal in farmAnimalList) {
                    if (farmAnimal.tag == farmAnimalTag) {
                        farmAnimalId = farmAnimal.id
                    }
                }
            }
        }
    }

    private fun setupQuantityTypeDropDown() {
        val adapter: ArrayAdapter<String> = ArrayAdapter(
            requireActivity(),
            android.R.layout.simple_dropdown_item_1line,
            getData()
        )
        binding.quantityType.setAdapter(adapter)

        binding.quantityTypeDropDown.setOnClickListener { binding.quantityType.showDropDown() }

        binding.quantityType.setOnClickListener { binding.quantityType.showDropDown() }
    }

    private fun getData() = mutableListOf(
        "KG", "TRAY", "LITRE"
    )
}

