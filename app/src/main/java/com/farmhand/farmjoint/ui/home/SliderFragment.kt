package com.farmhand.farmjoint.ui.home

import android.content.Context
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import com.farmhand.farmjoint.R
import com.farmhand.farmjoint.databinding.SliderListBinding
import timber.log.Timber
import java.io.IOException

class SliderFragment : Fragment() {

    private lateinit var sliderListBinding: SliderListBinding

    companion object {
        const val ARG_POSITION = "position"

        fun getInstance(position: Int): Fragment {
            val sliderFragment = SliderFragment()
            val bundle = Bundle()
            bundle.putInt(ARG_POSITION, position)
            sliderFragment.arguments = bundle
            return sliderFragment
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view =  inflater.inflate(R.layout.slider_list, container, false)
        sliderListBinding = SliderListBinding.bind(view)
        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        val position = requireArguments().getInt(ARG_POSITION)
        val imageFilePath = getString(R.string.slider_image_path, position)
//        val sliderNamesArray = requireContext().resources.getStringArray(R.array.slider_names)

        setImageFromAssetsFile(requireContext(), imageFilePath)
//        sliderListBinding.sliderNameTv.text = sliderNamesArray[position]
    }

    /**
     * Gets the file from assets, converts it into a bitmap and sets it on the ImageView
     * @param context a Context instance
     * @param filePath relative path of the file
     */
    private fun setImageFromAssetsFile(context: Context, filePath: String) {
        val imageBitmap: Bitmap?
        val assets = context.resources.assets
        try {
            val fileStream = assets.open(filePath)
            imageBitmap = BitmapFactory.decodeStream(fileStream)
            fileStream.close()
            sliderListBinding.sliderIv.setImageBitmap(imageBitmap)
        } catch (e: IOException) {
            Timber.d(e.message.toString())
            Toast.makeText(context, getString(R.string.image_loading_error), Toast.LENGTH_SHORT)
                .show()
        }
    }
}

