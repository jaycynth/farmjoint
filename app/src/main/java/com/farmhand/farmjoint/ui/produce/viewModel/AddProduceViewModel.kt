package com.farmhand.farmjoint.ui.produce.viewModel

import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.ViewModel
import androidx.lifecycle.liveData
import com.farmhand.farmjoint.data.repository.FarmAnimalRepository
import com.farmhand.farmjoint.data.repository.ProduceRepository
import com.farmhand.farmjoint.utils.Resource
import kotlinx.coroutines.Dispatchers
import okhttp3.RequestBody

class AddProduceViewModel @ViewModelInject constructor(
    private val repository: ProduceRepository, val farmAnimalRepo: FarmAnimalRepository
) : ViewModel() {

    fun allFarmAnimals(accessToken: String) = farmAnimalRepo.getAllFarmAnimals(accessToken)

    fun addProduce(produce: RequestBody, accessToken: String)  =
        liveData(Dispatchers.IO) {
            emit(Resource.loading())
            val responseStatus = repository.addProduce(
                requestBody = produce, accessToken = accessToken
            )

            if (responseStatus.status == Resource.Status.SUCCESS) {
                emit(Resource.success(responseStatus.data!!))
            } else if (responseStatus.status == Resource.Status.ERROR) {
                emit(Resource.error(responseStatus.message!!))
            }

        }
}
