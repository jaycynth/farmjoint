package com.farmhand.farmjoint.ui.register.viewModel

import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.ViewModel
import androidx.lifecycle.liveData
import com.farmhand.farmjoint.data.repository.UserRepository
import com.farmhand.farmjoint.utils.Resource
import kotlinx.coroutines.Dispatchers
import okhttp3.RequestBody

class RegisterViewModel @ViewModelInject constructor(private val repository: UserRepository) :
    ViewModel() {

    fun register(requestBody: RequestBody) =
        liveData(Dispatchers.IO) {
            emit(Resource.loading())
            val responseStatus = repository.register(
                requestBody = requestBody
            )

            if (responseStatus.status == Resource.Status.SUCCESS) {
                emit(Resource.success(responseStatus.data!!))
            } else if (responseStatus.status == Resource.Status.ERROR) {
                emit(Resource.error(responseStatus.message!!))
            }

        }
}