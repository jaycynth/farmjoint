package com.farmhand.farmjoint.ui.health.viewModel

import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.ViewModel
import androidx.lifecycle.liveData
import com.farmhand.farmjoint.data.repository.FarmAnimalRepository
import com.farmhand.farmjoint.data.repository.HealthRepository
import com.farmhand.farmjoint.utils.Resource
import kotlinx.coroutines.Dispatchers
import okhttp3.RequestBody

class UploadHealthRecordViewModel @ViewModelInject constructor(
    val farmAnimalRepo: FarmAnimalRepository, private val healthRepository: HealthRepository
) : ViewModel() {

    fun allFarmAnimals(accessToken:String) = farmAnimalRepo.getAllFarmAnimals(accessToken)
    fun allRecords(accessToken: String) = healthRepository.getAllHealth(accessToken)

    fun addHealthRecord(healthRecord: RequestBody, accessToken: String)  =
        liveData(Dispatchers.IO) {
            emit(Resource.loading())
            val responseStatus = healthRepository.addHealth(
                requestBody = healthRecord, accessToken = accessToken
            )

            if (responseStatus.status == Resource.Status.SUCCESS) {
                emit(Resource.success(responseStatus.data!!))
            } else if (responseStatus.status == Resource.Status.ERROR) {
                emit(Resource.error(responseStatus.message!!))
            }

        }
}