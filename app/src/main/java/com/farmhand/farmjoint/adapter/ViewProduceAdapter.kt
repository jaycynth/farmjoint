package com.farmhand.farmjoint.adapter

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.farmhand.farmjoint.data.model.Produce
import com.farmhand.farmjoint.databinding.ViewProduceLayoutBinding

class ViewProduceAdapter : RecyclerView.Adapter<ViewProduceAdapter.ViewProduceViewHolder>() {

    private val items = ArrayList<Produce>()


    fun setItems(items: ArrayList<Produce>) {
        this.items.clear()
        this.items.addAll(items)
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewProduceViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        val binding = ViewProduceLayoutBinding.inflate(inflater, parent, false)
        return ViewProduceViewHolder(binding)
    }

    override fun onBindViewHolder(holder: ViewProduceViewHolder, position: Int) =
        holder.bind(items[position])


    override fun getItemCount(): Int = items.size

    inner class ViewProduceViewHolder(private val binding: ViewProduceLayoutBinding):RecyclerView.ViewHolder(binding.root) {
        @SuppressLint("SetTextI18n")
        fun bind(produce: Produce) {
            binding.date.text = produce.date
            binding.productType.text = produce.produce_type
            binding.quantity.text = produce.quantity.toString()
            binding.eachPrice.text = "Ksh ${produce.each_price}"
            binding.totalAmount.text = "Ksh ${produce.total_price}"
            binding.quantityType.text = produce.quantity_type
        }

    }


}