package com.farmhand.farmjoint.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.farmhand.farmjoint.data.model.*
import com.farmhand.farmjoint.databinding.DateHeaderBinding
import com.farmhand.farmjoint.databinding.HealthRecordListItemBinding
import com.farmhand.farmjoint.utils.consolidateListH

class HealthRecordAdapter(private val listener: HealthRecordItemClickListener) :
    RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    private val listItem = ArrayList<ListItem>()


    fun setItems(items: ArrayList<HealthRecord>) {
        listItem.clear()
        listItem.addAll(consolidateListH(items))
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {

        var viewHolder: RecyclerView.ViewHolder? = null

        val inflater = LayoutInflater.from(parent.context)

        when (viewType) {
            ListItem.TYPE_GENERAL -> {
                val binding = HealthRecordListItemBinding.inflate(inflater, parent, false)
                viewHolder = HealthRecordViewHolder(binding)
            }
            ListItem.TYPE_DATE -> {
                val binding = DateHeaderBinding.inflate(inflater, parent, false)
                viewHolder = DateViewHolder(binding)
            }
        }
        return viewHolder!!
    }

    override fun onBindViewHolder(viewHolder: RecyclerView.ViewHolder, position: Int) {
        when (viewHolder.itemViewType) {

            ListItem.TYPE_GENERAL -> {
                val generalItem = listItem[position]
                val healthRecordViewHolder: HealthRecordViewHolder =
                    viewHolder as HealthRecordViewHolder

                healthRecordViewHolder.bind(generalItem, listener)
            }

            ListItem.TYPE_DATE -> {
                val dateItem = listItem[position]
                val dateViewHolder: DateViewHolder =
                    viewHolder as DateViewHolder

                dateViewHolder.bind(dateItem)

            }
        }
    }


    override fun getItemCount(): Int = listItem.size

    override fun getItemViewType(position: Int): Int {
        return listItem[position].getType()
    }

    class DateViewHolder(private val binding: DateHeaderBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun bind(item: ListItem) {
            binding.dateItem = item as DateItem
            binding.executePendingBindings()
        }

    }

    class HealthRecordViewHolder(private val binding: HealthRecordListItemBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun bind(item: ListItem, listener: HealthRecordItemClickListener) {
            binding.generalHealthRecord = item as GeneralHealthRecord
            binding.healthRecordItemClick = listener
            binding.executePendingBindings()
        }

    }

    interface HealthRecordItemClickListener {
        fun onClickHealthRecord(healthRecord: HealthRecord)
    }

}