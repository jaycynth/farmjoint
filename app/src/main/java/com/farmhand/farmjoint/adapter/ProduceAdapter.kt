package com.farmhand.farmjoint.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.farmhand.farmjoint.data.model.DateItem
import com.farmhand.farmjoint.data.model.GeneralItem
import com.farmhand.farmjoint.data.model.ListItem
import com.farmhand.farmjoint.data.model.Produce
import com.farmhand.farmjoint.databinding.DateHeaderBinding
import com.farmhand.farmjoint.databinding.ProduceListItemBinding
import com.farmhand.farmjoint.utils.consolidateList


class ProduceAdapter(
    private val listener: ProduceItemListener
) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    private val listItem = ArrayList<ListItem>()


    fun setItems(items: ArrayList<Produce>) {
        listItem.clear()
        listItem.addAll(consolidateList(items))
        notifyDataSetChanged()
    }


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        var viewHolder: RecyclerView.ViewHolder? = null

        val inflater = LayoutInflater.from(parent.context)

        when (viewType) {
            ListItem.TYPE_GENERAL -> {
                val binding = ProduceListItemBinding.inflate(inflater, parent, false)

                viewHolder = ProduceViewHolder(binding)
            }
            ListItem.TYPE_DATE -> {
                val binding = DateHeaderBinding.inflate(inflater, parent, false)
                viewHolder = DateViewHolder(binding)
            }
        }
        return viewHolder!!
    }

    override fun getItemCount(): Int = listItem.size

    override fun getItemViewType(position: Int): Int {
        return listItem[position].getType()
    }

    override fun onBindViewHolder(viewHolder: RecyclerView.ViewHolder, position: Int) {
        when (viewHolder.itemViewType) {
            ListItem.TYPE_GENERAL -> {
                val generalItem = listItem[position]
                val produceViewHolder: ProduceViewHolder = viewHolder as ProduceViewHolder

                produceViewHolder.bind(generalItem, listener)
            }

            ListItem.TYPE_DATE -> {
                val dateItem = listItem[position]
                val dateViewHolder: DateViewHolder = viewHolder as DateViewHolder

                dateViewHolder.bind(dateItem)

            }
        }
    }


    inner class DateViewHolder(private val binding: DateHeaderBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun bind(item: ListItem) {
            binding.dateItem = item as DateItem
            binding.executePendingBindings()
        }

    }

    inner class ProduceViewHolder(private val binding: ProduceListItemBinding) :
        RecyclerView.ViewHolder(binding.root) {

        fun bind(item: ListItem, listener: ProduceItemListener) {
            binding.generalItem = item as GeneralItem
            binding.produceItemClick = listener
            binding.executePendingBindings()
        }
    }

    interface ProduceItemListener {
        fun onClickedProduce(produce: Produce)
    }

}