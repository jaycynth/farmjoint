package com.farmhand.farmjoint.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.farmhand.farmjoint.databinding.ViewHolderCategoryBinding
import com.farmhand.farmjoint.data.model.Category


class CategoryAdapter(
    private val items: List<Category>,
    private val mListener: CategoryItemClickListener
) : RecyclerView.Adapter<CategoryAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        val binding = ViewHolderCategoryBinding.inflate(inflater,parent,false)
        return ViewHolder(binding)
    }

    override fun getItemCount(): Int = items.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) =
        holder.bind(items[position], mListener)


    inner class ViewHolder(private val binding: ViewHolderCategoryBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun bind(item: Category, mListener: CategoryItemClickListener) {
            binding.category = item
            binding.categoryItemClick = mListener
            binding.executePendingBindings()
        }
    }

    interface CategoryItemClickListener {
        fun onCategoryItemClicked(category: Category)
    }
}
