package com.farmhand.farmjoint.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.farmhand.farmjoint.data.model.Feed
import com.farmhand.farmjoint.data.model.Supplement
import com.farmhand.farmjoint.databinding.FeedListItemBinding
import com.farmhand.farmjoint.databinding.SupplementListItemBinding

class SupplementAdapter (private val listener: SupplementItemClickListener): RecyclerView.Adapter<SupplementAdapter.SupplementViewHolder>() {

    private val items = ArrayList<Supplement>()


    fun setItems(items: ArrayList<Supplement>) {
        this.items.clear()
        this.items.addAll(items)
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): SupplementAdapter.SupplementViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        val binding = SupplementListItemBinding.inflate(inflater, parent, false)
        return SupplementViewHolder(binding)
    }

    override fun onBindViewHolder(holder: SupplementAdapter.SupplementViewHolder, position: Int) =
        holder.bind(items[position], listener)


    override fun getItemCount(): Int = items.size

    inner class SupplementViewHolder(private val binding: SupplementListItemBinding): RecyclerView.ViewHolder(binding.root) {
        fun bind(item: Supplement, listener: SupplementItemClickListener) {
            binding.supplement = item
            binding.supplementItemClick = listener
            binding.executePendingBindings()
        }
    }

    interface SupplementItemClickListener{
        fun onClickSupplementItem(supplement: Supplement)
    }

}