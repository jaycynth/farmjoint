package com.farmhand.farmjoint.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.farmhand.farmjoint.data.model.FarmAnimal
import com.farmhand.farmjoint.databinding.FarmAnimalListItemBinding

class FarmAnimalAdapter(
    private val listener: FarmAnimalItemListener
) : RecyclerView.Adapter<FarmAnimalAdapter.FarmAnimalViewHolder>() {
    private val items = ArrayList<FarmAnimal>()
    private var mRecentlyDeletedItem: FarmAnimal? = null
    private var mRecentlyDeletedItemPosition = 0


    fun setItems(items: ArrayList<FarmAnimal>) {
        this.items.clear()
        this.items.addAll(items)
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): FarmAnimalAdapter.FarmAnimalViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        val binding = FarmAnimalListItemBinding.inflate(inflater, parent, false)
        return FarmAnimalViewHolder(binding)
    }

    override fun onBindViewHolder(holder: FarmAnimalAdapter.FarmAnimalViewHolder, position: Int) =
        holder.bind(items[position], listener)


    override fun getItemCount(): Int = items.size

    fun deleteItem(position: Int) {
        mRecentlyDeletedItem = items[position]
        mRecentlyDeletedItemPosition = position
        items.removeAt(position)
        notifyItemRemoved(position)
    }

    fun getCountedItemAtPosition(position: Int): FarmAnimal {
        return items[position]
    }


    inner class FarmAnimalViewHolder(private val binding: FarmAnimalListItemBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun bind(item: FarmAnimal, listener: FarmAnimalItemListener) {
            binding.farmAnimal = item
            binding.farmAnimalItemClick = listener
            binding.executePendingBindings()
        }
    }


    interface FarmAnimalItemListener {
        fun onClickFarmAnimal(farmAnimal: FarmAnimal)
    }

}
