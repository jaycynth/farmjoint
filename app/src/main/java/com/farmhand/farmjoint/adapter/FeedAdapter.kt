package com.farmhand.farmjoint.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.farmhand.farmjoint.adapter.FeedAdapter.FeedViewHolder
import com.farmhand.farmjoint.data.model.Feed
import com.farmhand.farmjoint.databinding.FeedListItemBinding

class FeedAdapter (private val listener:FeedItemClickListener): RecyclerView.Adapter<FeedViewHolder>() {

    private val items = ArrayList<Feed>()


    fun setItems(items: ArrayList<Feed>) {
        this.items.clear()
        this.items.addAll(items)
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): FeedViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        val binding = FeedListItemBinding.inflate(inflater, parent, false)
        return FeedViewHolder(binding)
    }

    override fun onBindViewHolder(holder: FeedViewHolder, position: Int) =
        holder.bind(items[position], listener)


    override fun getItemCount(): Int = items.size

    inner class FeedViewHolder(private val binding: FeedListItemBinding):RecyclerView.ViewHolder(binding.root) {
        fun bind(item: Feed, listener: FeedItemClickListener) {
            binding.feed = item
            binding.feedItemClick = listener
            binding.executePendingBindings()
        }

    }

     interface FeedItemClickListener{
        fun onClickFeed(feed: Feed)
    }

}