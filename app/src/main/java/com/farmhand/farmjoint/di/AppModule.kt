package com.farmhand.farmjoint.di

import android.annotation.SuppressLint
import android.content.Context
import com.farmhand.farmjoint.data.db.*
import com.farmhand.farmjoint.data.remote.*
import com.farmhand.farmjoint.data.repository.*
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ApplicationComponent
import dagger.hilt.android.qualifiers.ApplicationContext
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.security.SecureRandom
import java.security.cert.X509Certificate
import javax.inject.Singleton
import javax.net.ssl.*
import javax.security.cert.CertificateException


@Module
@InstallIn(ApplicationComponent::class)
object AppModule {

    private fun provideLogger(): OkHttpClient {
        return try {
            // Create a trust manager that does not validate certificate chains
            val trustAllCerts: Array<TrustManager> = arrayOf(
                object : X509TrustManager {
                    @SuppressLint("TrustAllX509TrustManager")
                    @Throws(CertificateException::class)
                    override fun checkClientTrusted(
                        chain: Array<X509Certificate?>?,
                        authType: String?
                    ) {
                    }

                    @SuppressLint("TrustAllX509TrustManager")
                    @Throws(CertificateException::class)
                    override fun checkServerTrusted(
                        chain: Array<X509Certificate?>?,
                        authType: String?
                    ) {
                    }

                    override fun getAcceptedIssuers(): Array<X509Certificate?> {
                        return arrayOf()
                    }
                }
            )

            // Install the all-trusting trust manager
            val sslContext: SSLContext = SSLContext.getInstance("SSL")
            sslContext.init(null, trustAllCerts, SecureRandom())

            // Create an ssl socket factory with our all-trusting manager
            val sslSocketFactory: SSLSocketFactory = sslContext.socketFactory

            val builder = OkHttpClient.Builder()
            builder.sslSocketFactory(sslSocketFactory, trustAllCerts[0] as X509TrustManager)
            builder.hostnameVerifier(HostnameVerifier { _, _ -> true })
            val interceptor = HttpLoggingInterceptor()
            interceptor.setLevel(HttpLoggingInterceptor.Level.BODY)
            builder.addInterceptor(interceptor)
            builder.build()
        } catch (e: Exception) {
            throw RuntimeException(e)
        }
    }

    @Singleton
    @Provides
    fun provideRetrofit(gson: Gson): Retrofit = Retrofit.Builder()
        .baseUrl("http://ac400c222fe5845d5aef6540fc38273c-1839162505.us-east-2.elb.amazonaws.com/api/")
        .client(provideLogger())
        .addConverterFactory(GsonConverterFactory.create(gson))
        .build()

    @Provides
    fun provideGson(): Gson = GsonBuilder().create()

    @Provides
    fun provideFarmJointService(retrofit: Retrofit): FarmJointService =
        retrofit.create(FarmJointService::class.java)

    @Singleton
    @Provides
    fun provideDatabase(@ApplicationContext appContext: Context) =
        AppDatabase.getDatabase(appContext)


    // Farm animal
    @Singleton
    @Provides
    fun provideFarmAnimalRemoteDataSource(farmJointService: FarmJointService) =
        FarmAnimalRemoteDataSource(farmJointService)

    @Singleton
    @Provides
    fun provideFarmAnimalDao(db: AppDatabase) = db.farmAnimalDao()

    @Singleton
    @Provides
    fun provideFarmAnimalRepository(
        remoteDataSource: FarmAnimalRemoteDataSource,
        localDataSource: FarmAnimalDao
    ) =
        FarmAnimalRepository(remoteDataSource, localDataSource)

    // Produce
    @Singleton
    @Provides
    fun provideProduceRemoteDataSource(farmJointService: FarmJointService) =
        ProduceRemoteDataSource(farmJointService)

    @Singleton
    @Provides
    fun provideProduceDao(db: AppDatabase) = db.produceDao()

    @Singleton
    @Provides
    fun provideProduceRepository(
        remoteDataSource: ProduceRemoteDataSource,
        localDataSource: ProduceDao
    ) =
        ProduceRepository(remoteDataSource, localDataSource)

    // feed
    @Singleton
    @Provides
    fun provideFeedRemoteDataSource(farmJointService: FarmJointService) =
        FeedRemoteDataSource(farmJointService)

    @Singleton
    @Provides
    fun provideFeedDao(db: AppDatabase) = db.feedDao()

    @Singleton
    @Provides
    fun provideFeedRepository(
        remoteDataSource: FeedRemoteDataSource,
        localDataSource: FeedDao
    ) =
        FeedRepository(remoteDataSource, localDataSource)


    //supplement
    @Singleton
    @Provides
    fun provideSupplementRemoteDataSource(farmJointService: FarmJointService) =
        SupplementRemoteService(farmJointService)

    @Singleton
    @Provides
    fun provideSupplementDao(db: AppDatabase) = db.supplementDao()

    @Singleton
    @Provides
    fun provideSupplementRepository(
        remoteDataSource: SupplementRemoteService,
        localDataSource: SupplementDao
    ) =
        SupplementRepository(remoteDataSource, localDataSource)

    // health
    @Singleton
    @Provides
    fun provideHealthRemoteDataSource(farmJointService: FarmJointService) =
        HealthRemoteService(farmJointService)

    @Singleton
    @Provides
    fun provideHealthDao(db: AppDatabase) = db.healthDao()

    @Singleton
    @Provides
    fun provideHealthRepository(
        remoteDataSource: HealthRemoteService,
        localDataSource: HealthDao
    ) =
        HealthRepository(remoteDataSource, localDataSource)


    //user
    @Singleton
    @Provides
    fun provideUserRemoteDataSource(farmJointService: FarmJointService) =
        UserRemoteDataSource(farmJointService)


    @Singleton
    @Provides
    fun provideUserRepository(remoteDataSource: UserRemoteDataSource) =
        UserRepository(remoteDataSource)

}