package com.farmhand.farmjoint.utils

import com.farmhand.farmjoint.data.model.*


private fun groupDataIntoHashMap(listOfProduce: ArrayList<Produce>): HashMap<String, ArrayList<Produce>> {
        val groupedHashMap: HashMap<String, ArrayList<Produce>> = HashMap()

        for (produce in listOfProduce) {
            val hashMapKey = produce.date
            if (groupedHashMap.containsKey(hashMapKey)) {
                // The key is already in the HashMap; add the produce object
                // against the existing key.
                groupedHashMap[hashMapKey]?.add(produce)
            } else {
                // The key is not there in the HashMap; create a new key-value pair
                val list:ArrayList<Produce> = ArrayList()
                list.add(produce)
                groupedHashMap[hashMapKey] = list
            }
        }
        return groupedHashMap
    }


    fun consolidateList(list: ArrayList<Produce>): ArrayList<ListItem> {
        val consolidatedList: ArrayList<ListItem> = ArrayList()

        val groupedHashMap = groupDataIntoHashMap(list)
        for (date in groupedHashMap.keys) {
            val dateItem = DateItem(date = date)
            consolidatedList.add(dateItem)
            for (produce in groupedHashMap[date]!!) {
                val generalItem = GeneralItem(produce = produce)
                consolidatedList.add(generalItem)
            }
        }
        return consolidatedList
    }

private fun groupDataIntoHashMapH(listOfProduce: ArrayList<HealthRecord>): HashMap<String, ArrayList<HealthRecord>> {
    val groupedHashMap: HashMap<String, ArrayList<HealthRecord>> = HashMap()

    for (produce in listOfProduce) {
        val hashMapKey = produce.date
        if (groupedHashMap.containsKey(hashMapKey)) {
            // The key is already in the HashMap; add the produce object
            // against the existing key.
            groupedHashMap[hashMapKey]?.add(produce)
        } else {
            // The key is not there in the HashMap; create a new key-value pair
            val list:ArrayList<HealthRecord> = ArrayList()
            list.add(produce)
            groupedHashMap[hashMapKey] = list
        }
    }
    return groupedHashMap
}


fun consolidateListH(list: ArrayList<HealthRecord>): ArrayList<ListItem> {
    val consolidatedList: ArrayList<ListItem> = ArrayList()

    val groupedHashMap = groupDataIntoHashMapH(list)
    for (date in groupedHashMap.keys) {
        val dateItem = DateItem(date = date)
        consolidatedList.add(dateItem)
        for (produce in groupedHashMap[date]!!) {
            val generalItem = GeneralHealthRecord(healthRecord = produce)
            consolidatedList.add(generalItem)
        }
    }
    return consolidatedList
}
