package com.farmhand.farmjoint.utils

import android.content.Context
import android.content.SharedPreferences

class SharedPrefs(ctx : Context) {
    private val PREF_NAME: String = "shared_pref"

    private val prefs: SharedPreferences = ctx.getSharedPreferences(PREF_NAME, 0)


    fun saveUserID(userID: String) {
        val editor = prefs.edit()
        editor.putString(Constants.KEY_USER_TOKEN, userID)
        editor.apply()
    }

    fun getUserID(): String {
        return prefs.getString(Constants.KEY_USER_TOKEN, null).toString()
    }

    fun saveUserName(userName: String) {
        val editor = prefs.edit()
        editor.putString(Constants.KEY_USER_NAME, userName)
        editor.apply()
    }

    fun getUserName(): String {
        return prefs.getString(Constants.KEY_USER_NAME, null).toString()
    }

}