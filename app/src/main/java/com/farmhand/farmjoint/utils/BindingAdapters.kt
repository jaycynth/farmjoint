package com.farmhand.farmjoint.utils

import android.widget.TextView
import androidx.annotation.DrawableRes
import androidx.databinding.BindingAdapter


object BindingAdapters {
    @JvmStatic
    @BindingAdapter("app:srcVector")
    fun setSrcVector(view: TextView, @DrawableRes drawable: Int) {
        view.setCompoundDrawablesWithIntrinsicBounds(drawable, 0, 0, 0)
    }

}