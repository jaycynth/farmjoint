package com.farmhand.farmjoint.utils

object Constants {

    const val KEY_USER_TOKEN= "user_token"
    const val KEY_USER_NAME = "user_name"
    val SUPPLEMENT_URL =
        "https://a1c6ac8e0fc734a818c4b505ab39a462-409866986.us-east-2.elb.amazonaws.com:80/api/"
    val PRODUCE_URL =
        "https://a40468f7f8eff4e92948575c3884271c-882199298.us-east-2.elb.amazonaws.com:80/api/"
    val FEED_URL =
        "https://ac23c21a06f54430294822a1ce448316-1208776722.us-east-2.elb.amazonaws.com:70/api/"
    val FARM_ANIMAL_URL =
        "https://ad47d0543548d4b6a92d3cf116a8df0d-1751817712.us-east-2.elb.amazonaws.com:80/api/"
    val HEALTH_URL =
        "https://aaadf0239a887408db4d8746d38a294e-957390028.us-east-2.elb.amazonaws.com:80/api/"
}