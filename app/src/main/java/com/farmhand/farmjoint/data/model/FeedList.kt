package com.farmhand.farmjoint.data.model

data class FeedList(
    val next_page_token: String,
    val feed_list: List<Feed>
)