package com.farmhand.farmjoint.data.model

import androidx.room.Entity
import androidx.room.PrimaryKey
import com.google.gson.annotations.SerializedName

@Entity
data class Produce(
    @PrimaryKey
    @SerializedName("produce_id")
    val id: String,
    val date: String,
    val each_price: Int,
    val farmanimal_id: String,
    val produce_type: String,
    val quantity: Int,
    val quantity_type: String,
    val total_price: Int
)