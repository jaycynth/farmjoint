package com.farmhand.farmjoint.data.model

data class Login(
    val jwt_token: String,
    val user: User
)