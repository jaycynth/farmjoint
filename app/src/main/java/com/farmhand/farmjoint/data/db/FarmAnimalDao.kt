package com.farmhand.farmjoint.data.db

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.farmhand.farmjoint.data.model.FarmAnimal

@Dao
interface FarmAnimalDao {
    @Query("SELECT * FROM farmAnimal")
    fun getAllFarmAnimal() : LiveData<List<FarmAnimal>>

    @Query("SELECT * FROM farmAnimal WHERE id = :id")
    fun getFarmAnimal(id: Int): LiveData<FarmAnimal>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertAll(farmAnimalList: List<FarmAnimal>?)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insert(farmAnimal: FarmAnimal)
}