package com.farmhand.farmjoint.data.remote

import com.farmhand.farmjoint.data.model.*
import okhttp3.RequestBody
import retrofit2.Response
import retrofit2.http.*

interface FarmJointService {
    @GET("produce")
    suspend fun getProduceList(@Header("Authorization") accessToken: String): Response<ProduceList>

    @GET("produce")
    suspend fun getProduceListForFarmAnimal(
        @Header("Authorization") accessToken: String,
        @Query("farmanimal_id") farmAnimalId: String
    ): Response<ProduceList>

    @POST("produce")
    suspend fun addProduce(
        @Header("Authorization") accessToken: String,
        @Body requestBody: RequestBody
    ): Response<AddProduce>

    @GET("farmanimal")
    suspend fun getFarmAnimalList(@Header("Authorization") accessToken: String): Response<FarmAnimalList>

    @POST("farmanimal")
    suspend fun addFarmAnimal(
        @Body requestBody: RequestBody,
        @Header("Authorization") accessToken: String
    ): Response<AddFarmAnimal>

    @DELETE("farmanimal")
    suspend fun deleteFarmAnimal(@Header("Authorization") accessToken: String,
                                 @Query("farmanimal_id") farmAnimalId: String): Response<DeleteFarmAnimal>

    @GET("feed")
    suspend fun getFeedList(@Header("Authorization") accessToken: String): Response<FeedList>

    @POST("feed")
    suspend fun addFeed(
        @Body requestBody: RequestBody,
        @Header("Authorization") accessToken: String
    ): Response<Feed>

    @GET("supplement")
    suspend fun getSupplementList(@Header("Authorization") accessToken: String): Response<SupplementList>

    @POST("supplement")
    suspend fun addSupplement(
        @Body requestBody: RequestBody,
        @Header("Authorization") accessToken: String
    ): Response<Supplement>

    @GET("health")
    suspend fun getHealthList(@Header("Authorization") accessToken: String): Response<HealthList>

    @POST("health")
    suspend fun addHealthRecord(
        @Body requestBody: RequestBody,
        @Header("Authorization") accessToken: String
    ): Response<AddHealthRecord>

    @POST("users/action/login")
    suspend fun login(@Body requestBody: RequestBody): Response<Login>

    @POST("users")
    suspend fun register(@Body requestBody: RequestBody): Response<User>


}