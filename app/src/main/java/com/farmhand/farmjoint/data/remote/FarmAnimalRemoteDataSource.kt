package com.farmhand.farmjoint.data.remote

import okhttp3.RequestBody
import javax.inject.Inject

class FarmAnimalRemoteDataSource @Inject constructor(
    private val farmJointService: FarmJointService
) : BaseDataSource() {

    suspend fun getFarmAnimalList(accessToken: String) =
        getResult { farmJointService.getFarmAnimalList(accessToken) }

    suspend fun addFarmAnimal(farmAnimal: RequestBody, accessToken: String) =
        getResult {
            farmJointService.addFarmAnimal(requestBody = farmAnimal, accessToken = accessToken)
        }

    suspend fun deleteFarmAnimal(accessToken: String, farmAnimalId: String) =
        getResult {
            farmJointService.deleteFarmAnimal(accessToken, farmAnimalId)
        }
}