package com.farmhand.farmjoint.data.remote

import okhttp3.RequestBody
import javax.inject.Inject

class ProduceRemoteDataSource @Inject constructor(
    private val farmJointService: FarmJointService
) : BaseDataSource() {

    suspend fun getProduceList(accessToken: String) =
        getResult { farmJointService.getProduceList(accessToken) }

    suspend fun getProduceListForFarmanimal(accessToken: String, farmanimalId:String) =
        getResult { farmJointService.getProduceListForFarmAnimal(accessToken,farmanimalId) }

    suspend fun addProduce(produce: RequestBody, accessToken: String) = getResult {
        farmJointService.addProduce(requestBody = produce, accessToken = accessToken)
    }

}