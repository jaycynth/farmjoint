package com.farmhand.farmjoint.data.remote

import okhttp3.RequestBody
import javax.inject.Inject

class SupplementRemoteService @Inject constructor(
    private val farmJointService: FarmJointService
) : BaseDataSource() {

    suspend fun getSupplementList(accessToken: String) = getResult { farmJointService.getSupplementList(accessToken) }

    suspend fun addSupplement(supplement: RequestBody,
        accessToken: String
    ) = getResult {
        farmJointService.addSupplement(
            requestBody = supplement, accessToken = accessToken
        )
    }

}