package com.farmhand.farmjoint.data.model

data class HealthList(
    val next_page_token: String,
    val health_list: List<HealthRecord>
)
