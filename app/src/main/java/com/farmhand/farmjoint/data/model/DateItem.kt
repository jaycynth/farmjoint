package com.farmhand.farmjoint.data.model

data class DateItem(val date: String) : ListItem() {
    override fun getType(): Int {
        return TYPE_DATE
    }

}