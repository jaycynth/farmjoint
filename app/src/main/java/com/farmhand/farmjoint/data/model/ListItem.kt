package com.farmhand.farmjoint.data.model

abstract class ListItem {
    abstract fun getType(): Int

    companion object {
        const val TYPE_DATE = 0
        const val TYPE_GENERAL = 1

    }
}