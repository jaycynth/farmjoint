package com.farmhand.farmjoint.data.model

data class GeneralItem(val produce: Produce) : ListItem() {
    override fun getType(): Int {
        return TYPE_GENERAL
    }
}