package com.farmhand.farmjoint.data.model

data class SupplementList(
    val next_page_token: String,
    val supplement_list: List<Supplement>
)