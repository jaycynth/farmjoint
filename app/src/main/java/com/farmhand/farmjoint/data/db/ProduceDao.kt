package com.farmhand.farmjoint.data.db

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.farmhand.farmjoint.data.model.Produce

@Dao
interface ProduceDao {
    @Query("SELECT * FROM produce")
    fun getAllProduce() : LiveData<List<Produce>>

    @Query("SELECT * FROM produce WHERE id = :id")
    fun getProduce(id: Int): LiveData<Produce>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertAll(produceList: List<Produce>?)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insert(produce: Produce)
}