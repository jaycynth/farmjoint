package com.farmhand.farmjoint.data.repository
import com.farmhand.farmjoint.data.db.FarmAnimalDao
import com.farmhand.farmjoint.data.remote.FarmAnimalRemoteDataSource
import com.farmhand.farmjoint.utils.performGetOperation
import kotlinx.coroutines.coroutineScope
import okhttp3.RequestBody
import javax.inject.Inject
class FarmAnimalRepository @Inject constructor(
    private val remoteDataSource: FarmAnimalRemoteDataSource,
    private val localDataSource: FarmAnimalDao
) {

    fun getAllFarmAnimals(accessToken: String) = performGetOperation(
        databaseQuery = { localDataSource.getAllFarmAnimal() },
        networkCall = { remoteDataSource.getFarmAnimalList(accessToken) },
        saveCallResult = { localDataSource.insertAll(it.farmanimal_list) }
    )



    suspend fun addFarmAnimal(requestBody: RequestBody, accessToken: String) = coroutineScope {
        remoteDataSource.addFarmAnimal(farmAnimal = requestBody,accessToken = accessToken)
    }

    suspend fun deleteFarmAnimal(accessToken: String, farmAnimalId: String) = coroutineScope {
        remoteDataSource.deleteFarmAnimal(accessToken, farmAnimalId)
    }
}