package com.farmhand.farmjoint.data.remote

import okhttp3.RequestBody
import javax.inject.Inject

class UserRemoteDataSource @Inject constructor(
    private val farmJointService: FarmJointService
) : BaseDataSource() {


    suspend fun login(requestBody: RequestBody) = getResult {
        farmJointService.login(requestBody)
    }

    suspend fun register(requestBody: RequestBody) = getResult {
        farmJointService.register(requestBody)
    }


}