package com.farmhand.farmjoint.data.repository


import com.farmhand.farmjoint.data.db.ProduceDao
import com.farmhand.farmjoint.data.remote.ProduceRemoteDataSource
import com.farmhand.farmjoint.utils.performGetOperation
import kotlinx.coroutines.coroutineScope
import okhttp3.RequestBody
import javax.inject.Inject

class ProduceRepository @Inject constructor(
    private val remoteDataSource: ProduceRemoteDataSource,
    private val localDataSource: ProduceDao
) {

    fun getAllProduce(accessToken: String) = performGetOperation(
        databaseQuery = { localDataSource.getAllProduce() },
        networkCall = { remoteDataSource.getProduceList(accessToken) },
        saveCallResult = { localDataSource.insertAll(it.produce_list) }
    )

    suspend fun getProduceListForFarmAnimal(accessToken: String, farmanimalId: String) =
        coroutineScope { remoteDataSource.getProduceListForFarmanimal(accessToken, farmanimalId) }

    suspend fun addProduce(requestBody: RequestBody, accessToken: String)
        = coroutineScope {
        remoteDataSource.addProduce(produce = requestBody, accessToken = accessToken)
    }
}