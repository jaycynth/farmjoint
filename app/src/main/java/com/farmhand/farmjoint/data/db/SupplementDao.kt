package com.farmhand.farmjoint.data.db

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.farmhand.farmjoint.data.model.Supplement

@Dao
interface SupplementDao {
    @Query("SELECT * FROM supplement")
    fun getAllSupplement() : LiveData<List<Supplement>>

    @Query("SELECT * FROM supplement WHERE id = :id")
    fun getSupplement(id: Int): LiveData<Supplement>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertAll(supplementList: List<Supplement>?)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insert(supplement: Supplement)
}