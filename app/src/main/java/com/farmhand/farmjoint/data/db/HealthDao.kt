package com.farmhand.farmjoint.data.db

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.farmhand.farmjoint.data.model.HealthRecord

@Dao
interface HealthDao{
    @Query("SELECT * FROM healthRecord")
    fun getAllHealthRecord() : LiveData<List<HealthRecord>>

    @Query("SELECT * FROM healthRecord WHERE id = :id")
    fun getHealthRecord(id: Int): LiveData<HealthRecord>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertAll(healthRecordList: List<HealthRecord>?)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insert(healthRecord: HealthRecord)
}