package com.farmhand.farmjoint.data.model

import android.widget.ImageView
import androidx.databinding.BindingAdapter
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.farmhand.farmjoint.R
import com.google.gson.annotations.SerializedName

@Entity
data class FarmAnimal(
    @PrimaryKey
    @SerializedName("farmanimal_id")
    val id: String,
    val photo: String?,
    val status: String,
    val tag: String,
    val calfingCount: String,
    val birth_date: String
)

@BindingAdapter("animalPhoto")
fun loadImage(view: ImageView, photo: String?) {
    Glide.with(view.context)
        .load(photo).apply(RequestOptions().circleCrop())
        .placeholder(R.drawable.ic_farm_animal_placeholder)
        .into(view)
}
