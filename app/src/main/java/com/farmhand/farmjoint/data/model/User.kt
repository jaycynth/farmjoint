package com.farmhand.farmjoint.data.model

data class User(
    val user_id: String,
    val user_name: String,
    val phone_number: String

) {
    constructor(user_name: String, phone_number: String) : this(
        user_name = user_name, phone_number = phone_number, user_id = ""
    )
}