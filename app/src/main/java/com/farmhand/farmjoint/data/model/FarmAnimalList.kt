package com.farmhand.farmjoint.data.model

data class FarmAnimalList(
    val farmanimal_list: List<FarmAnimal>,
    val next_page_token: String
)