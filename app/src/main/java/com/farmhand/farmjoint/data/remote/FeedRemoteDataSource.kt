package com.farmhand.farmjoint.data.remote

import okhttp3.RequestBody
import javax.inject.Inject

class FeedRemoteDataSource @Inject constructor(
    private val farmJointService: FarmJointService
) : BaseDataSource() {

    suspend fun getFeedList(accessToken: String) =
        getResult { farmJointService.getFeedList(accessToken) }

    suspend fun addFeed(
        feed: RequestBody,
        accessToken: String
    ) = getResult {
        farmJointService.addFeed(
            requestBody = feed, accessToken = accessToken
        )
    }

}