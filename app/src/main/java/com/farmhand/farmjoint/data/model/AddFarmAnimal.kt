package com.farmhand.farmjoint.data.model

data class AddFarmAnimal(
    val farmanimal: FarmAnimal,
    val success_message: String
)