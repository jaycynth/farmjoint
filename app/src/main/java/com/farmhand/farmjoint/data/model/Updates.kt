package com.farmhand.farmjoint.data.model


data class Update(val imageUrl: String, val caption: String, val story: String)