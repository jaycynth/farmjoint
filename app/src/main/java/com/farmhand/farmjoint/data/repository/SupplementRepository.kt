package com.farmhand.farmjoint.data.repository

import com.farmhand.farmjoint.data.db.SupplementDao
import com.farmhand.farmjoint.data.remote.SupplementRemoteService
import com.farmhand.farmjoint.utils.performGetOperation
import kotlinx.coroutines.coroutineScope
import okhttp3.RequestBody
import javax.inject.Inject

class SupplementRepository @Inject constructor(
    private val remoteDataSource: SupplementRemoteService,
    private val localDataSource: SupplementDao
) {

    fun getAllSupplement(accessToken: String) = performGetOperation(
        databaseQuery = { localDataSource.getAllSupplement() },
        networkCall = { remoteDataSource.getSupplementList(accessToken) },
        saveCallResult = { localDataSource.insertAll(it.supplement_list) }
    )

    suspend fun addSupplement(requestBody: RequestBody, accessToken: String) = coroutineScope {
        remoteDataSource.addSupplement(supplement = requestBody, accessToken = accessToken)

    }
}