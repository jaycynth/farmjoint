package com.farmhand.farmjoint.data.model

import com.farmhand.farmjoint.R

data class Category(val name: String, val image: Int)

fun getData() = mutableListOf(
    Category("Farm Animals", R.drawable.ic_farmanimal),
    Category("Produce Records",R.drawable.ic_produce),
    Category("Feed Records",R.drawable.ic_feed),
    Category("Health Records",R.drawable.ic_health_record),
    Category("Supplement Records", R.drawable.ic_feed)
)