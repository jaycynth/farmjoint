package com.farmhand.farmjoint.data.model

data class GeneralHealthRecord(val healthRecord: HealthRecord) : ListItem() {
    override fun getType(): Int {
        return TYPE_GENERAL
    }
}