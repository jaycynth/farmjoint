package com.farmhand.farmjoint.data.model

import androidx.room.Entity
import androidx.room.PrimaryKey
import com.google.gson.annotations.SerializedName

@Entity
data class Feed (
    val date: String,
    @PrimaryKey
    @SerializedName("feed_id")
    val id: String,
    val name: String,
    val price: String,
    val quantity: String
)
