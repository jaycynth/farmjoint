package com.farmhand.farmjoint.data.model

data class AddProduce(
    val produce: Produce,
    val success_message: String
)