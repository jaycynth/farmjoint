package com.farmhand.farmjoint.data.model

import androidx.room.Entity
import androidx.room.PrimaryKey
import com.google.gson.annotations.SerializedName

@Entity
data class HealthRecord(
    @PrimaryKey
    @SerializedName("health_id")
    val id: String,
    val date: String,
    val description: String,
    val farmanimal_id: String,
    val procedure_type: String,
    val vet_name: String
)
