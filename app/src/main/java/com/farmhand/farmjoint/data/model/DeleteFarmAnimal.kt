package com.farmhand.farmjoint.data.model

data class DeleteFarmAnimal(
    val success_message: String
)