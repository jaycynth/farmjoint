package com.farmhand.farmjoint.data.db

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import com.farmhand.farmjoint.data.model.*

@Database(entities = [FarmAnimal::class, Produce::class,HealthRecord::class, Supplement::class, Feed::class], version = 21, exportSchema = false)
abstract class AppDatabase : RoomDatabase() {

    abstract fun farmAnimalDao(): FarmAnimalDao
    abstract fun produceDao(): ProduceDao
    abstract fun healthDao(): HealthDao
    abstract fun supplementDao(): SupplementDao
    abstract fun feedDao(): FeedDao

    companion object {
        @Volatile
        private var instance: AppDatabase? = null

        fun getDatabase(context: Context): AppDatabase =
            instance ?: synchronized(this) {
                instance ?: buildDatabase(context).also {
                    instance = it
                }
            }

        private fun buildDatabase(appContext: Context) =
            Room.databaseBuilder(appContext, AppDatabase::class.java, "farmjoint")
                .fallbackToDestructiveMigration()
                .build()
    }

}