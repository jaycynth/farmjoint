package com.farmhand.farmjoint.data.model

data class ProduceList(
    val produce_list: List<Produce>,
    val next_page_token: String
    )