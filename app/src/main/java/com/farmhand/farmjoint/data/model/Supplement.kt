package com.farmhand.farmjoint.data.model

import androidx.room.Entity
import androidx.room.PrimaryKey
import com.google.gson.annotations.SerializedName

@Entity
data class Supplement(
    val price: String,
    val quantity: String,
    @PrimaryKey
    @SerializedName("supplement_id")
    val id: String,
    val supplement_name: String,
    val units: String,
    val date_bought: String
)
