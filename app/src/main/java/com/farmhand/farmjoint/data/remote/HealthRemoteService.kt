package com.farmhand.farmjoint.data.remote

import okhttp3.RequestBody
import javax.inject.Inject

class HealthRemoteService @Inject constructor(
    private val farmJointService: FarmJointService
) : BaseDataSource() {

    suspend fun getHealthList(accessToken: String) = getResult { farmJointService.getHealthList(accessToken) }

    suspend fun addHealth(
        healthRecord: RequestBody,
        accessToken: String
    ) = getResult {
        farmJointService.addHealthRecord(
            requestBody = healthRecord,
            accessToken = accessToken
        )
    }

}