package com.farmhand.farmjoint.data.repository

import com.farmhand.farmjoint.data.db.HealthDao
import com.farmhand.farmjoint.data.remote.HealthRemoteService
import com.farmhand.farmjoint.utils.performGetOperation
import kotlinx.coroutines.coroutineScope
import okhttp3.RequestBody
import javax.inject.Inject

class HealthRepository @Inject constructor(
    private val remoteDataSource: HealthRemoteService,
    private val localDataSource: HealthDao
) {

    fun getAllHealth(accessToken: String) = performGetOperation(
        databaseQuery = { localDataSource.getAllHealthRecord() },
        networkCall = { remoteDataSource.getHealthList(accessToken) },
        saveCallResult = { localDataSource.insertAll(it.health_list) }
    )

    suspend fun addHealth(requestBody: RequestBody, accessToken: String) = coroutineScope {
        remoteDataSource.addHealth(healthRecord = requestBody, accessToken = accessToken)

    }
}