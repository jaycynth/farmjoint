package com.farmhand.farmjoint.data.db

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.farmhand.farmjoint.data.model.Feed

@Dao
interface FeedDao {
    @Query("SELECT * FROM feed")
    fun getAllFeed() : LiveData<List<Feed>>

    @Query("SELECT * FROM feed WHERE id = :id")
    fun getFeed(id: Int): LiveData<Feed>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertAll(feedList: List<Feed>?)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insert(feed: Feed)
}