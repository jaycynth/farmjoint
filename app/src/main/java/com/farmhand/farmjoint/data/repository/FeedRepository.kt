package com.farmhand.farmjoint.data.repository

import com.farmhand.farmjoint.data.db.FeedDao
import com.farmhand.farmjoint.data.remote.FeedRemoteDataSource
import com.farmhand.farmjoint.utils.performGetOperation
import kotlinx.coroutines.coroutineScope
import okhttp3.RequestBody
import javax.inject.Inject

class FeedRepository @Inject constructor(
    private val remoteDataSource: FeedRemoteDataSource,
    private val localDataSource: FeedDao
) {

    fun getAllFeed(accessToken: String) = performGetOperation(
        databaseQuery = { localDataSource.getAllFeed() },
        networkCall = { remoteDataSource.getFeedList(accessToken) },
        saveCallResult = { localDataSource.insertAll(it.feed_list) }
    )

    suspend fun addFeed(requestBody: RequestBody, accessToken: String) = coroutineScope {
        remoteDataSource.addFeed(feed = requestBody, accessToken = accessToken)

    }
}