package com.farmhand.farmjoint.data.repository

import com.farmhand.farmjoint.data.remote.UserRemoteDataSource
import kotlinx.coroutines.coroutineScope
import okhttp3.RequestBody
import javax.inject.Inject

class UserRepository @Inject constructor(
    private val remoteDataSource: UserRemoteDataSource
) {

    suspend fun login(requestBody: RequestBody) = coroutineScope {
        remoteDataSource.login(requestBody = requestBody)
    }

   suspend fun register(requestBody: RequestBody) = coroutineScope {
       remoteDataSource.register(requestBody = requestBody)
   }
}
