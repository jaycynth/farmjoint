package com.farmhand.farmjoint

import android.app.Application
import android.content.Context
import androidx.viewbinding.BuildConfig
import com.farmhand.farmjoint.utils.SharedPrefs
import dagger.hilt.android.HiltAndroidApp
import timber.log.Timber

@HiltAndroidApp
class FarmJoint : Application(){

    init {
        instance = this
    }

    companion object {
        private var instance: FarmJoint? = null

        fun applicationContext(): Context {
            return instance!!.applicationContext
        }
        var prefs: SharedPrefs? = null

    }

    override fun onCreate() {
        super.onCreate()

        prefs = SharedPrefs(applicationContext)


        if (BuildConfig.DEBUG) {
            Timber.plant(Timber.DebugTree())
        }
    }
}